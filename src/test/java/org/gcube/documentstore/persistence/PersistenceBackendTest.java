/**
 * 
 */
package org.gcube.documentstore.persistence;

import java.io.File;
import java.util.Calendar;

import org.gcube.accounting.datamodel.UsageRecord;
import org.gcube.accounting.datamodel.usagerecords.TestUsageRecord;
import org.gcube.accounting.persistence.AccountingPersistenceFactory;
import org.gcube.testutility.ContextTest;
import org.gcube.testutility.StressTestUtility;
import org.gcube.testutility.TestOperation;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class PersistenceBackendTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(PersistenceBackendTest.class);
	
	public static PersistenceBackend getPersistence() throws Exception{
		AccountingPersistenceFactory.initAccountingPackages();
		PersistenceBackendFactory.setFallbackLocation(null);
		return PersistenceBackendFactory.getPersistenceBackend(ContextTest.getCurrentContext());
	}
	
	@Test
	public void singleTestNoScope() throws Exception {
		PersistenceBackendFactory.setFallbackLocation(null);
		final PersistenceBackend persistence = PersistenceBackendFactory.getPersistenceBackend(null);
		Assert.assertTrue(persistence instanceof FallbackPersistenceBackend);
		StressTestUtility.stressTest(new TestOperation() {
			@Override
			public void operate(int i) {
				UsageRecord usageRecord = TestUsageRecord.createTestServiceUsageRecord();
				persistence.accountValidateAggregate(usageRecord, true, false);
			}
		}, 1);
		
		persistence.flush();
	}
	
	@Test
	public void singleTest() throws Exception {
		final PersistenceBackend persistence = getPersistence();
		StressTestUtility.stressTest(new TestOperation() {
			@Override
			public void operate(int i) {
				UsageRecord usageRecord = TestUsageRecord.createTestServiceUsageRecord();
				persistence.accountValidateAggregate(usageRecord, true, false);
			}
		}, 1);
		
		persistence.flush();
	}
	
	@Test
	public void stressTestNoAggregation() throws Exception {
		final PersistenceBackend persistence = getPersistence();
		StressTestUtility.stressTest(new TestOperation() {
			@Override
			public void operate(int i) {
				UsageRecord usageRecord = TestUsageRecord.createTestServiceUsageRecord();
				persistence.accountValidateAggregate(usageRecord, true, false);
			}
		});
	}
	
	@Test
	public void stressTestWithAggregation() throws Exception {
		final PersistenceBackend persistence = getPersistence();
		
		StressTestUtility.stressTest(new TestOperation() {
			@Override
			public void operate(int i) throws Exception {
				UsageRecord usageRecord = TestUsageRecord.createTestServiceUsageRecord();
				persistence.account(usageRecord);
			}
		});
		persistence.flush();
	}
	
	
	@Test
	public void testScopeRecheck() throws Exception {
		logger.debug("Going to check First Time");
		PersistenceBackend first = getPersistence();
		logger.debug("First {} : {}", PersistenceBackend.class.getSimpleName(), first);
		
		long startTime = Calendar.getInstance().getTimeInMillis();
		long endTime = startTime;
		
		while(endTime <=  (startTime + (PersistenceBackendFactory.FALLBACK_RETRY_TIME + 2100))){
			endTime = Calendar.getInstance().getTimeInMillis();
		}

		logger.debug("Going to check Second Time");
		PersistenceBackend second = getPersistence();
		logger.debug("Second {} : {}", PersistenceBackend.class.getSimpleName(), second);

	}
	
	
	@Test
	public void test() {
		File f = new File("test");
		PersistenceBackend fallbackPersistenceBackend = new FallbackPersistenceBackend(f);
		
		if(fallbackPersistenceBackend instanceof FallbackPersistenceBackend) {
			logger.trace("{} is an instance of {}", fallbackPersistenceBackend, FallbackPersistenceBackend.class.getSimpleName());
		}
		
		PersistenceBackend rediscovered = new PersistenceAccountingService();
		
		if(!rediscovered.getClass().equals(fallbackPersistenceBackend.getClass())){
			logger.debug("New {} found : {}", PersistenceBackend.class.getSimpleName(), rediscovered.getClass().getSimpleName());
		}else {
			logger.trace("{} is still a {}. We will see if next time we will be more lucky.", 
					PersistenceBackend.class.getSimpleName(),
					FallbackPersistenceBackend.class.getSimpleName());
		}
	}
	
	
	
	
}
