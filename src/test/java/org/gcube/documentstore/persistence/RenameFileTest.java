/**
 * 
 */
package org.gcube.documentstore.persistence;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

import org.gcube.accounting.datamodel.usagerecords.TestUsageRecord;
import org.gcube.accounting.persistence.AccountingPersistence;
import org.gcube.accounting.persistence.AccountingPersistenceFactory;
import org.gcube.testutility.ContextTest;
import org.junit.Test;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class RenameFileTest extends ContextTest {
	
	private final static String ELABORATION_FILE_SUFFIX = ".ELABORATION";
	
	public void printLine(File file, String line) throws Exception {
		synchronized (file) {
			try(FileWriter fw = new FileWriter(file, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter out = new PrintWriter(bw)){
					out.println(line);
					out.flush();
			} catch( IOException e ){
			   throw e;
			}
		}
	}
	
	@Test
	public void renameFileTest() throws Exception{
		File first = new File("./test.txt");
		Long timestamp = Calendar.getInstance().getTimeInMillis();
		File elaborationFile = new File(first.getAbsolutePath() + ELABORATION_FILE_SUFFIX + "." + timestamp.toString());
		first.renameTo(elaborationFile);
		
		printLine(first, "-FIRST-");
		for(int i=0; i<100; i++){
			printLine(elaborationFile, "-ELABORATION-" +  i);
			printLine(first, "-FIRST MOVED-"+i);
		}
		
	}
	
	@Test
	public void testPersistenceBackendMonitor() throws Exception{
		AccountingPersistenceFactory.setFallbackLocation(".");
		AccountingPersistence accountingPersistence = AccountingPersistenceFactory.getPersistence();
		accountingPersistence.account(TestUsageRecord.createTestStorageUsageRecord());
		
	}
	
}
