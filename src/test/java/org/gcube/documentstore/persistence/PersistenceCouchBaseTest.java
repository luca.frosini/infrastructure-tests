/**
 * 
 */
package org.gcube.documentstore.persistence;

import org.gcube.accounting.datamodel.usagerecords.TestUsageRecord;
import org.gcube.accounting.persistence.AccountingPersistenceFactory;
import org.gcube.common.authorization.client.exceptions.ObjectNotFound;
import org.gcube.documentstore.records.DSMapper;
import org.gcube.documentstore.records.Record;
import org.gcube.testutility.ContextTest;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;


/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class PersistenceCouchBaseTest extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(PersistenceCouchBaseTest.class);
	
	
	@Test
	public void persistenceIsCouchBase() throws ObjectNotFound, Exception {
		PersistenceBackendFactory.setFallbackLocation(null);
		FallbackPersistenceBackend fallbackPersistenceBackend = PersistenceBackendFactory.createFallback(ContextTest.getCurrentContext());
		PersistenceBackend persistenceBackend = PersistenceBackendFactory.rediscoverPersistenceBackend(fallbackPersistenceBackend, ContextTest.getCurrentContext());
		Assert.assertTrue(persistenceBackend instanceof PersistenceCouchBase);
	}

	@Test
	public void persistenceIsCouchBaseForcingImmediateRediscovery() throws ObjectNotFound, Exception {
		
		AccountingPersistenceFactory.initAccountingPackages();
		
		PersistenceBackendFactory.setFallbackLocation(null);
		String context = ContextTest.getCurrentContext();
		PersistenceBackendFactory.forceImmediateRediscovery(context);
		PersistenceBackend persistenceBackend = PersistenceBackendFactory.getPersistenceBackend(context);
		Assert.assertTrue(persistenceBackend instanceof PersistenceCouchBase);
		
	}
	
	
	@Test
	public void testJsonNodeUsageRecordConversions() throws Exception {
		Record record = TestUsageRecord.createTestServiceUsageRecord();
		logger.debug("UsageRecord : {}", DSMapper.marshal(record));
		JsonNode node = PersistenceCouchBase.usageRecordToJsonNode(record);
		logger.debug("Node : {}", node.toString());
		Record r = PersistenceCouchBase.jsonNodeToUsageRecord(node);
		Assert.assertEquals(0, record.compareTo(r));
		Assert.assertEquals(0, r.compareTo(record));
	}
	
}