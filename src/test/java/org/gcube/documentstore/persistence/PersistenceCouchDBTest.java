///**
// * 
// */
//package org.gcube.documentstore.persistence;
//
//import java.net.URL;
//
//import org.codehaus.jackson.JsonNode;
//import org.gcube.documentstore.records.Record;
//import org.gcube.testutility.ScopedTest;
//import org.gcube.testutility.TestUsageRecord;
//import org.junit.Assert;
//import org.junit.Test;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
///**
// * @author Luca Frosini (ISTI - CNR)
// *
// */
//public class PersistenceCouchDBTest extends ScopedTest {
//	
//	private static final Logger logger = LoggerFactory.getLogger(PersistenceCouchDBTest.class);
//	
//	@Test
//	public void testJsonNodeUsageRecordConversions() throws Exception {
//		Record record = TestUsageRecord.createTestServiceUsageRecord();
//		logger.debug("UsageRecord : {}", record.toString());
//		JsonNode node = PersistenceCouchDB.usageRecordToJsonNode(record);
//		logger.debug("Node : {}", node.toString());
//		Record r = PersistenceCouchDB.jsonNodeToUsageRecord(node);
//		Assert.assertEquals(0, record.compareTo(r));
//		Assert.assertEquals(0, r.compareTo(record));
//	}
//	
//	@Test
//	public void testJsonNodeUsageRecordConversionsWithNestedMap() throws Exception {
//		Record record = TestUsageRecord.createTestTaskUsageRecord();
//		logger.debug("UsageRecord : {}", record.toString());
//		JsonNode node = PersistenceCouchDB.usageRecordToJsonNode(record);
//		logger.debug("Node : {}", node.toString());
//		Record r = PersistenceCouchDB.jsonNodeToUsageRecord(node);
//		Assert.assertEquals(0, record.compareTo(r));
//		Assert.assertEquals(0, r.compareTo(record));
//	}
//	
//	//@Test
//	public void testProxyWithTestConfiguration() throws Exception{
//		// Production-Preproduction Nodes
//		//URL url = new URL("http://accounting-d4s.d4science.org");
//		//URL url = new URL("http://couchdb02-d4s.d4science.org:5984");
//		//URL url = new URL("http://couchdb01-d4s.d4science.org:5984");
//		
//		URL url = new URL("http://accounting-d-d4s.d4science.org/_utils/");
//		//URL url = new URL("http://couchdb02-d-d4s.d4science.org:5984");
//		//URL url = new URL("http://couchdb01-d-d4s.d4science.org:5984");
//		
//		PersistenceBackendConfiguration persitenceConfiguration = PersistenceBackendConfiguration.getUnconfiguredInstance();
//		persitenceConfiguration.addProperty(PersistenceCouchDB.URL_PROPERTY_KEY, url.toString());
//		persitenceConfiguration.addProperty(PersistenceCouchDB.USERNAME_PROPERTY_KEY, "");
//		persitenceConfiguration.addProperty(PersistenceCouchDB.PASSWORD_PROPERTY_KEY, "");
//		persitenceConfiguration.addProperty(PersistenceCouchDB.DB_NAME,"");
//		
//		PersistenceCouchDB couch = new PersistenceCouchDB();
//		couch.prepareConnection(persitenceConfiguration);
//		
//		Record record = TestUsageRecord.createTestServiceUsageRecord();
//		couch.reallyAccount(record);
//		
//	}
//	
//}