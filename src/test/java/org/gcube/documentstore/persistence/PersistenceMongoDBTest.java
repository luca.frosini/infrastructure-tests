///**
// * 
// */
//package org.gcube.documentstore.persistence;
//
//import java.net.URL;
//
//import org.bson.Document;
//import org.gcube.accounting.datamodel.UsageRecord;
//import org.gcube.accounting.datamodel.basetypes.AbstractStorageUsageRecord.OperationType;
//import org.gcube.documentstore.records.Record;
//import org.gcube.testutility.TestUsageRecord;
//import org.junit.Assert;
//import org.junit.Test;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
///**
// * @author Luca Frosini (ISTI - CNR)
// *
// */
//public class PersistenceMongoDBTest {
//	
//	private static final Logger logger = LoggerFactory.getLogger(PersistenceMongoDBTest.class);
//	
//	
//	@Test
//	public void testJsonNodeUsageRecordConversions() throws Exception {
//		Record record = TestUsageRecord.createTestServiceUsageRecord();
//		logger.debug("UsageRecord : {}", record.toString());
//		Document document = PersistenceMongoDB.usageRecordToDocument(record);
//		logger.debug("Document : {}", document.toString());
//		Record r = PersistenceMongoDB.documentToUsageRecord(document);
//		Assert.assertEquals(0, record.compareTo(r));
//		Assert.assertEquals(0, r.compareTo(record));
//	}
//	
//	@Test
//	public void testJsonNodeUsageRecordConversionsWithNestedMap() throws Exception {
//		Record record = TestUsageRecord.createTestTaskUsageRecord();
//		logger.debug("UsageRecord : {}", record.toString());
//		Document document = PersistenceMongoDB.usageRecordToDocument(record);
//		logger.debug("Document : {}", document.toString());
//		Record r = PersistenceMongoDB.documentToUsageRecord(document);
//		Assert.assertEquals(0, record.compareTo(r));
//		Assert.assertEquals(0, r.compareTo(record));
//	}
//	
//	public enum AUX {
//		TEST, TESTER, TESTING
//	}
//	
//	@Test
//	public void testConfiguration() throws Exception{
//		URL url = new URL("http://mongo-test.d4science.org");
//		
//		PersistenceBackendConfiguration persitenceConfiguration = PersistenceBackendConfiguration.getUnconfiguredInstance();
//		persitenceConfiguration.addProperty(PersistenceMongoDB.URL_PROPERTY_KEY, url.toString());
//		persitenceConfiguration.addProperty(PersistenceMongoDB.USERNAME_PROPERTY_KEY, "accounting");
//		persitenceConfiguration.addProperty(PersistenceMongoDB.PASSWORD_PROPERTY_KEY, "");
//		persitenceConfiguration.addProperty(PersistenceMongoDB.DB_NAME,"accounting");
//		persitenceConfiguration.addProperty(PersistenceMongoDB.COLLECTION_NAME, UsageRecord.class.getSimpleName());
//		PersistenceMongoDB mongo = new PersistenceMongoDB();
//		mongo.prepareConnection(persitenceConfiguration);
//		
//
//		Record record = TestUsageRecord.createTestServiceUsageRecord();
//		record.setResourceProperty("Test", AUX.TESTER);
//		mongo.reallyAccount(record);
//		
//		record = TestUsageRecord.createTestStorageUsageRecord();
//		record.setResourceProperty("Test", AUX.TESTER);
//		mongo.reallyAccount(record);
//		
//		record = TestUsageRecord.createTestJobUsageRecord();
//		mongo.reallyAccount(record);
//		
//	}
//	
//	@Test
//	public void test(){
//		EnumCodec<OperationType> enumCodec = new EnumCodec<OperationType>(OperationType.class);
//		Assert.assertEquals(OperationType.class, enumCodec.getEncoderClass());
//	}
//
//}