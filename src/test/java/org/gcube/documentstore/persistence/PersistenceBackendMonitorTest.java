/**
 * 
 */
package org.gcube.documentstore.persistence;

import java.util.concurrent.TimeUnit;

import org.gcube.accounting.datamodel.UsageRecord;
import org.gcube.accounting.datamodel.usagerecords.TestUsageRecord;
import org.gcube.documentstore.exception.InvalidValueException;
import org.gcube.testutility.ContextTest;
import org.gcube.testutility.StressTestUtility;
import org.gcube.testutility.TestOperation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class PersistenceBackendMonitorTest extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(PersistenceBackendMonitorTest.class);
	
	public static final long timeout = 5000;
	public static final TimeUnit timeUnit = TimeUnit.MILLISECONDS;
	
	@Test
	public void parsingTest() throws Exception {
		PersistenceBackendFactory.setFallbackLocation(null);
		final PersistenceBackend persistence = PersistenceBackendFactory.getPersistenceBackend(ContextTest.getCurrentContext());
		
		StressTestUtility.stressTest(new TestOperation() {
			
			@Override
			public void operate(int i) {
				UsageRecord usageRecord = null;
				switch (i%3) {
					case 0:
						usageRecord = TestUsageRecord.createTestServiceUsageRecord();
						break;
					case 1:
						usageRecord = TestUsageRecord.createTestStorageUsageRecord();
						break;
					case 2:
						usageRecord = TestUsageRecord.createTestJobUsageRecord();
						break;
				}
				try {
					persistence.account(usageRecord);
				} catch (InvalidValueException e) {
					throw new RuntimeException(e);
				}
			}
		}, 3);
		
		logger.debug(" START -----------------------------------------------");
		logger.debug("Flushing the buffered records");
		persistence.flush();
		logger.debug(" END -----------------------------------------------");
		
		PersistenceBackend persistenceBackend = PersistenceBackendFactory.getPersistenceBackend(ContextTest.getCurrentContext());
		
		if(persistenceBackend instanceof FallbackPersistenceBackend) {
			/* This line has no sense in real scenario is only used to avoid null pointer exception in test because 
			 * the test has no time to get the real persistence
			 */
			persistenceBackend.setFallback((FallbackPersistenceBackend) persistenceBackend);
		}
		
		FallbackMonitor fallbackMonitor = new FallbackMonitor(persistenceBackend, false); 
		fallbackMonitor.run();
		
		logger.debug("{} finished", FallbackMonitor.class.getSimpleName());
	}
	
	@Test
	public void singleParsingTest() throws Exception {
		PersistenceBackendFactory.setFallbackLocation(null);
		PersistenceBackend persistenceBackend = PersistenceBackendFactory.getPersistenceBackend(ContextTest.getCurrentContext());
		if(persistenceBackend instanceof FallbackPersistenceBackend) {
			/* This line has no sense in real scenario is only used to avoid null pointer exception in test because 
			 * the test has no time to get the real persistence
			 */
			persistenceBackend.setFallback((FallbackPersistenceBackend) persistenceBackend);
		}
		FallbackMonitor fallbackMonitor = new FallbackMonitor(persistenceBackend); 
		fallbackMonitor.run();
	}
	
}
