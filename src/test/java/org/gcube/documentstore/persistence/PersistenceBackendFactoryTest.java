/**
 * 
 */
package org.gcube.documentstore.persistence;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class PersistenceBackendFactoryTest {
	
	private static final Logger logger = LoggerFactory.getLogger(PersistenceBackendFactoryTest.class);
	
	@Test
	public void parsingTest() throws Exception {
		PersistenceBackend persistenceBackend = PersistenceBackendFactory.getPersistenceBackend(null);
		logger.debug("{}", persistenceBackend);
		PersistenceBackendFactory.flushAll();
		PersistenceBackendFactory.flush(null);
		persistenceBackend.flush();
	}
}
