/**
 * 
 */
package org.gcube.documentstore.records;


import org.gcube.accounting.datamodel.aggregation.AggregatedServiceUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.ServiceUsageRecord;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class RecordUtilityTest {

	private static Logger logger = LoggerFactory.getLogger(RecordUtilityTest.class);
	
	@Test
	public void test() {
		Assert.assertTrue(Record.class.isAssignableFrom(ServiceUsageRecord.class));
		Assert.assertFalse(AggregatedRecord.class.isAssignableFrom(ServiceUsageRecord.class));
		Assert.assertTrue(Record.class.isAssignableFrom(AggregatedServiceUsageRecord.class));
		Assert.assertTrue(AggregatedRecord.class.isAssignableFrom(AggregatedServiceUsageRecord.class));
	}
	
	@Test
	public void testNewReflection() {
		RecordUtility.addRecordPackage(ServiceUsageRecord.class.getPackage());
		logger.trace("{}", RecordUtility.recordClassesFound);
		RecordUtility.addRecordPackage(AggregatedServiceUsageRecord.class.getPackage());
		logger.trace("{}", RecordUtility.aggregatedRecordClassesFound);
		logger.trace("{}", RecordUtility.recordAggregationMapping);
	}
	
	
	
	
	
}
