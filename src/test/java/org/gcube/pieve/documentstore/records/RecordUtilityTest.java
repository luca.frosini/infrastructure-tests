/**
 * 
 */
package org.gcube.pieve.documentstore.records;


import java.net.URI;

import org.gcube.accounting.datamodel.UsageRecord.OperationResult;
import org.gcube.accounting.datamodel.aggregation.AggregatedServiceUsageRecord;
import org.gcube.accounting.datamodel.aggregation.AggregatedStorageUsageRecord;
import org.gcube.accounting.datamodel.basetypes.AbstractStorageUsageRecord.DataType;
import org.gcube.accounting.datamodel.basetypes.AbstractStorageUsageRecord.OperationType;
import org.gcube.accounting.datamodel.usagerecords.ServiceUsageRecord;
import org.gcube.documentstore.records.AggregatedRecord;
import org.gcube.documentstore.records.Record;
import org.gcube.documentstore.records.RecordUtility;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class RecordUtilityTest {

	private static Logger logger = LoggerFactory.getLogger(RecordUtilityTest.class);
	
	@Test
	public void testImportOldRecord() throws Exception {
		String line = "{"
				+ "resourceScope=/gcube/devsec, "
				+ "scope=/gcube/devsec, "
				+ "operationCount=1, "
				+ "usageRecordType=StorageUsageRecord, "
				+ "endTime=1448447153009, "
				+ "consumerId=CSV, "
				+ "startTime=1448447153009, "
				+ "id=c7bab219-4024-4019-a8ad-ff5f342b439b, "
				+ "dataVolume=68, "
				+ "dataType=STORAGE, "
				+ "resourceOwner=CSV, "
				+ "operationResult=SUCCESS, "
				+ "resourceURI=testprotocol://objectURI, "
				+ "operationType=CREATE, "
				+ "aggregated=true, "
				+ "creationTime=1448447153096, "
				+ "providerURI=data.d4science.org}";
		
		logger.debug(line);
		
		
		Record record = RecordUtility.getRecord(line);
		Assert.assertTrue(record instanceof AggregatedStorageUsageRecord);
		AggregatedStorageUsageRecord aggregatedStorageUsageRecord = (AggregatedStorageUsageRecord) record;
		
		Assert.assertTrue(aggregatedStorageUsageRecord.getResourceScope().compareTo("/gcube/devsec")==0);
		Assert.assertTrue(aggregatedStorageUsageRecord.getScope().compareTo("/gcube/devsec")==0);
		Assert.assertTrue(aggregatedStorageUsageRecord.getOperationCount()==1);
		// 
		Assert.assertTrue(aggregatedStorageUsageRecord.getRecordType().compareTo("StorageUsageRecord")==0);
		Assert.assertTrue(aggregatedStorageUsageRecord.getEndTime().getTimeInMillis()==(new Long("1448447153009")));
		Assert.assertTrue(aggregatedStorageUsageRecord.getConsumerId().compareTo("CSV")==0);
		Assert.assertTrue(aggregatedStorageUsageRecord.getStartTime().getTimeInMillis()==(new Long("1448447153009")));
		Assert.assertTrue(aggregatedStorageUsageRecord.getId().compareTo("c7bab219-4024-4019-a8ad-ff5f342b439b")==0);
		Assert.assertTrue(aggregatedStorageUsageRecord.getDataVolume()==68);
		Assert.assertTrue(aggregatedStorageUsageRecord.getDataType()==DataType.STORAGE);
		Assert.assertTrue(aggregatedStorageUsageRecord.getResourceOwner().compareTo("CSV")==0);
		Assert.assertTrue(aggregatedStorageUsageRecord.getOperationResult()==OperationResult.SUCCESS);
		Assert.assertTrue(aggregatedStorageUsageRecord.getResourceURI().compareTo(new URI("testprotocol://objectURI"))==0);
		Assert.assertTrue(aggregatedStorageUsageRecord.getOperationType()==OperationType.CREATE);
		Assert.assertTrue((Boolean) aggregatedStorageUsageRecord.getResourceProperty(AggregatedStorageUsageRecord.AGGREGATED));
		Assert.assertTrue(aggregatedStorageUsageRecord.getCreationTime().getTimeInMillis()==(new Long("1448447153096")));
		Assert.assertTrue(aggregatedStorageUsageRecord.getProviderURI().compareTo(new URI("data.d4science.org"))==0);
		
		
		logger.debug("{}", aggregatedStorageUsageRecord);
	}
	
	@Test
	public void test() {
		Assert.assertTrue(Record.class.isAssignableFrom(ServiceUsageRecord.class));
		Assert.assertFalse(AggregatedRecord.class.isAssignableFrom(ServiceUsageRecord.class));
		Assert.assertTrue(Record.class.isAssignableFrom(AggregatedServiceUsageRecord.class));
		Assert.assertTrue(AggregatedRecord.class.isAssignableFrom(AggregatedServiceUsageRecord.class));
	}
	
	@Test
	public void testNewReflection() {
		RecordUtility.addRecordPackage(ServiceUsageRecord.class.getPackage());
		logger.trace("{}", RecordUtility.recordClassesFound);
		RecordUtility.addRecordPackage(AggregatedServiceUsageRecord.class.getPackage());
		logger.trace("{}", RecordUtility.aggregatedRecordClassesFound);
		logger.trace("{}", RecordUtility.recordAggregationMapping);
	}
	
}
