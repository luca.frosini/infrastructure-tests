/**
 * 
 */
package org.gcube.pieve.documentstore.persistence;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.gcube.accounting.datamodel.UsageRecord;
import org.gcube.accounting.persistence.AccountingPersistenceFactory;
import org.gcube.documentstore.persistence.FallbackPersistenceBackend;
import org.gcube.documentstore.persistence.PersistenceBackend;
import org.gcube.documentstore.persistence.PersistenceBackendFactory;
import org.gcube.pieve.testutility.ScopedTest;
import org.gcube.pieve.testutility.StressTestUtility;
import org.gcube.pieve.testutility.TestOperation;
import org.gcube.pieve.testutility.TestUsageRecord;
import org.gcube.pieve.testutility.TestUtility;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class PersistenceBackendTest extends ScopedTest {
	
	private static Logger logger = LoggerFactory.getLogger(PersistenceBackendTest.class);
	
	public static final long timeout = 5000;
	public static final TimeUnit timeUnit = TimeUnit.MILLISECONDS;
	
	public static PersistenceBackend getPersistence(){
		AccountingPersistenceFactory.initAccountingPackages();
		PersistenceBackendFactory.setFallbackLocation(null);
		return PersistenceBackendFactory.getPersistenceBackend(TestUtility.getScope());
	}
	
	@Test
	public void singleTestNoScope() throws Exception {
		PersistenceBackendFactory.setFallbackLocation(null);
		final PersistenceBackend persistence = PersistenceBackendFactory.getPersistenceBackend(null);
		Assert.assertTrue(persistence instanceof FallbackPersistenceBackend);
		StressTestUtility.stressTest(new TestOperation() {
			@Override
			public void operate(int i) {
				UsageRecord usageRecord = TestUsageRecord.createTestServiceUsageRecord();
				persistence.accountValidateAggregate(usageRecord, true, false);
			}
		}, 1);
		
		persistence.flush(timeout, timeUnit);
	}
	
	@Test
	public void singleTest() throws Exception {
		final PersistenceBackend persistence = getPersistence();
		StressTestUtility.stressTest(new TestOperation() {
			@Override
			public void operate(int i) {
				UsageRecord usageRecord = TestUsageRecord.createTestServiceUsageRecord();
				persistence.accountValidateAggregate(usageRecord, true, false);
			}
		}, 1);
		
		persistence.flush(timeout, timeUnit);
	}
	
	@Test
	public void stressTestNoAggregation() throws Exception {
		final PersistenceBackend persistence = getPersistence();
		StressTestUtility.stressTest(new TestOperation() {
			@Override
			public void operate(int i) {
				UsageRecord usageRecord = TestUsageRecord.createTestServiceUsageRecord();
				persistence.accountValidateAggregate(usageRecord, true, false);
			}
		});
	}
	
	@Test
	public void stressTestWithAggregation() throws Exception {
		final PersistenceBackend persistence = getPersistence();
		
		StressTestUtility.stressTest(new TestOperation() {
			@Override
			public void operate(int i) throws Exception {
				UsageRecord usageRecord = TestUsageRecord.createTestServiceUsageRecord();
				persistence.account(usageRecord);
			}
		});
		persistence.flush(timeout, timeUnit);
	}
	
	
	@Test
	public void testScopeRecheck() throws Exception {
		logger.debug("Going to check First Time");
		PersistenceBackend first = getPersistence();
		logger.debug("First {} : {}", PersistenceBackend.class.getSimpleName(), first);
		
		long startTime = Calendar.getInstance().getTimeInMillis();
		long endTime = startTime;
		
		while(endTime <=  (startTime + (PersistenceBackendFactory.FALLBACK_RETRY_TIME + 2100))){
			endTime = Calendar.getInstance().getTimeInMillis();
		}

		logger.debug("Going to check Second Time");
		PersistenceBackend second = getPersistence();
		logger.debug("Second {} : {}", PersistenceBackend.class.getSimpleName(), second);

	}
	
}
