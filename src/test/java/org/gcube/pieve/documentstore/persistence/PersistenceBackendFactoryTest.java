/**
 * 
 */
package org.gcube.pieve.documentstore.persistence;

import java.util.concurrent.TimeUnit;

import org.gcube.documentstore.persistence.PersistenceBackend;
import org.gcube.documentstore.persistence.PersistenceBackendFactory;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class PersistenceBackendFactoryTest {
	
	private static final Logger logger = LoggerFactory.getLogger(PersistenceBackendFactoryTest.class);
	
	@Test
	public void parsingTest() throws Exception {
		PersistenceBackend persistenceBackend = PersistenceBackendFactory.getPersistenceBackend(null);
		logger.debug("{}", persistenceBackend);
		PersistenceBackendFactory.flushAll(100, TimeUnit.MILLISECONDS);
		PersistenceBackendFactory.flush(null, 100, TimeUnit.MILLISECONDS);
		persistenceBackend.flush(100, TimeUnit.MILLISECONDS);
	}
}
