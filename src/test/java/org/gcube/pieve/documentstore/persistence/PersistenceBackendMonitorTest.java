/**
 * 
 */
package org.gcube.pieve.documentstore.persistence;

import java.util.concurrent.TimeUnit;

import org.gcube.accounting.datamodel.UsageRecord;
import org.gcube.documentstore.exception.InvalidValueException;
import org.gcube.documentstore.persistence.FallbackPersistenceBackend;
import org.gcube.documentstore.persistence.PersistenceBackend;
import org.gcube.documentstore.persistence.PersistenceBackendFactory;
import org.gcube.pieve.testutility.ScopedTest;
import org.gcube.pieve.testutility.StressTestUtility;
import org.gcube.pieve.testutility.TestOperation;
import org.gcube.pieve.testutility.TestUsageRecord;
import org.gcube.pieve.testutility.TestUtility;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class PersistenceBackendMonitorTest extends ScopedTest {
	
	private static final Logger logger = LoggerFactory.getLogger(PersistenceBackendMonitorTest.class);
	
	public static final long timeout = 5000;
	public static final TimeUnit timeUnit = TimeUnit.MILLISECONDS;
	
	@Test
	public void parsingTest() throws Exception {
		PersistenceBackendFactory.setFallbackLocation(null);
		final PersistenceBackend persistence = PersistenceBackendFactory.getPersistenceBackend(TestUtility.getScope());
		
		StressTestUtility.stressTest(new TestOperation() {
			
			@Override
			public void operate(int i) {
				UsageRecord usageRecord = null;
				switch (i%3) {
					case 0:
						usageRecord = TestUsageRecord.createTestServiceUsageRecord();
						break;
					case 1:
						usageRecord = TestUsageRecord.createTestStorageUsageRecord();
						break;
					case 2:
						usageRecord = TestUsageRecord.createTestJobUsageRecord();
						break;
				}
				try {
					persistence.account(usageRecord);
				} catch (InvalidValueException e) {
					throw new RuntimeException(e);
				}
			}
		});
		
		logger.debug(" START -----------------------------------------------");
		logger.debug("Flushing the buffered records");
		persistence.flush(timeout, timeUnit);
		logger.debug(" END -----------------------------------------------");
		
		PersistenceBackend persistenceBackend = PersistenceBackendFactory.getPersistenceBackend(TestUtility.getScope());
		persistenceBackend.setFallback((FallbackPersistenceBackend) persistenceBackend);
		PersistenceBackendMonitor temporalDataPersistenceBackendMonitor = new PersistenceBackendMonitor(persistenceBackend); 
		
		temporalDataPersistenceBackendMonitor.run();
		
	}
	
	@Test
	public void singleParsingTest() throws Exception {
		PersistenceBackendFactory.setFallbackLocation(null);
		PersistenceBackend persistenceBackend = PersistenceBackendFactory.getPersistenceBackend(TestUtility.getScope());
		//persistenceBackend.setFallback((FallbackPersistenceBackend) persistenceBackend);
		PersistenceBackendMonitor temporalDataPersistenceBackendMonitor = new PersistenceBackendMonitor(persistenceBackend); 
		
		temporalDataPersistenceBackendMonitor.run();
	}
	
}
