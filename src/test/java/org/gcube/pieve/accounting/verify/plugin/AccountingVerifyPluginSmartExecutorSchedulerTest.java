/**
 * 
 */
package org.gcube.pieve.accounting.verify.plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.gcube.pieve.testutility.ScopedTest;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.client.plugins.ExecutorPlugin;
import org.gcube.vremanagement.executor.client.proxies.SmartExecutorProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccountingVerifyPluginSmartExecutorSchedulerTest extends ScopedTest {

	private static Logger logger = LoggerFactory.getLogger(AccountingVerifyPluginSmartExecutorSchedulerTest.class);

	private SmartExecutorProxy proxy;
	@Before
	public void before() throws Exception{
		super.before();
		//ScopeProvider.instance.reset(); // Comment this to run the test. this line has been added to avoid unwanted launch
		//SecurityTokenProvider.instance.set(TestUtility.TOKEN);
		//ScopeProvider.instance.set("/gcube/devNext");
		proxy = ExecutorPlugin.getExecutorProxy("Accouting-Verify-Insert-Couchbase-Plugin").build();		
		Assert.assertNotNull(proxy);
	
	}
	
	public UUID scheduleTest(Scheduling scheduling) throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		logger.debug("Inputs : {}", inputs);
		inputs.put("interval",4);
		LaunchParameter parameter = new LaunchParameter("Accouting-Verify-Insert-Couchbase-Plugin", inputs);
		parameter.setScheduling(scheduling);
		
		try {
			String uuidString = proxy.launch(parameter);
			return UUID.fromString(uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}
		
	}
	@Test
	public void LaunchTest() throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		logger.debug("Inputs : {}", inputs);
		
		inputs.put("interval",4);
		//specify a recovery 0 default recovery and aggregate, 1 only aggregate, 2 only recovery
	
		LaunchParameter parameter = new LaunchParameter("Accouting-Verify-Insert-Couchbase-Plugin", inputs);
		//parameter.setScheduling(scheduling);
		try {
			String uuidString = proxy.launch(parameter);
			logger.debug("Launched with UUID : {}", uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}
		
	}
	
	
	@Test
	public void cronExpPreviousMustBeTerminated() throws Exception {
		//CronExpression cronExpression = new CronExpression("0 10 10 * * ?"); // every day at 10:10
		CronExpression cronExpression = new CronExpression("0 0 0/3 * * ?");
		
		Scheduling scheduling = new Scheduling(cronExpression, true);
		scheduling.setGlobal(true);
		UUID uuid = scheduleTest(scheduling);
		logger.debug("Launched with UUID : {}", uuid);
	}
	
		
	
	@Test
	public void unSchedule() throws Exception {
		proxy.unSchedule(null, true);
		
	}
	
	@Test
		public void stop() throws Exception {
			proxy.stop("Accouting-Aggregator-Plugin");
		}
	
}
