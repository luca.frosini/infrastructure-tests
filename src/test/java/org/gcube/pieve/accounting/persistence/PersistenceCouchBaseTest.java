/**
 * 
 */
package org.gcube.pieve.accounting.persistence;

import java.io.StringWriter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.gcube.accounting.persistence.AccountingPersistenceConfiguration;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.Resources;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.documentstore.persistence.PersistenceBackend;
import org.gcube.documentstore.persistence.PersistenceBackendFactory;
//import org.gcube.documentstore.persistence.PersistenceCouchBase;
import org.gcube.informationsystem.publisher.RegistryPublisher;
import org.gcube.informationsystem.publisher.RegistryPublisherFactory;
import org.gcube.pieve.documentstore.persistence.PersistenceCouchBase;
import org.gcube.pieve.testutility.ScopedTest;
import org.gcube.pieve.testutility.TestUtility;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class PersistenceCouchBaseTest extends ScopedTest {
	
	private static final Logger logger = LoggerFactory.getLogger(PersistenceCouchBaseTest.class);
	
	public static final long timeout = 5000;
	public static final TimeUnit timeUnit = TimeUnit.MILLISECONDS;
	
	
	public static PersistenceBackend getPersistence(){
		PersistenceBackendFactory.setFallbackLocation(null);
		return PersistenceBackendFactory.getPersistenceBackend(TestUtility.getScope());
	}
	
	@Test
	public void persistenceIsCouchBase() {
		PersistenceBackend persistence = getPersistence();
		Assert.assertTrue(persistence instanceof PersistenceCouchBase);
	}
	
	private static void publishResource(Resource resource) throws Exception {
		StringWriter stringWriter = new StringWriter();
		Resources.marshal(resource, stringWriter);
		
		RegistryPublisher registryPublisher = RegistryPublisherFactory.create();
		
	    try {
	    	logger.debug("Trying to publish to {}:\n{}", TestUtility.getScope(), stringWriter);
	    	registryPublisher.create(resource);
		} catch (Exception e) {
			logger.error("The resource was not published", e);
			throw e;
		}
	}
	
	private static void unPublishResource(Resource resource) throws Exception {
		//StringWriter stringWriter = new StringWriter();
		//Resources.marshal(resource, stringWriter);
		
		RegistryPublisher registryPublisher = RegistryPublisherFactory.create();
	    
		String id = resource.id();
	    logger.debug("Trying to remove {} with ID {} from {}", 
	    		resource.getClass().getSimpleName(), id, 
	    		TestUtility.getScope());
	    
		registryPublisher.remove(resource);
		
		logger.debug("{} with ID {} removed successfully", resource.getClass().getSimpleName(), id);
	}
	
	public void testScopeRecheck() throws Exception {
		ServiceEndpoint serviceEndpoint = null;
		try {
			AccountingPersistenceConfiguration persitenceConfiguration = new AccountingPersistenceConfiguration(PersistenceCouchBase.class);
			serviceEndpoint = persitenceConfiguration.getServiceEndpoint(
					AccountingPersistenceConfiguration.SERVICE_ENDPOINT_CATEGORY, 
					AccountingPersistenceConfiguration.SERVICE_ENDPOINT_NAME, 
					PersistenceCouchBase.class);
			unPublishResource(serviceEndpoint);
		}catch(IndexOutOfBoundsException e){
			SecurityTokenProvider.instance.set(TestUtility.PARENT_TOKEN);
			AccountingPersistenceConfiguration persitenceConfiguration = new AccountingPersistenceConfiguration(PersistenceCouchBase.class);
			serviceEndpoint = persitenceConfiguration.getServiceEndpoint(
					AccountingPersistenceConfiguration.SERVICE_ENDPOINT_CATEGORY, AccountingPersistenceConfiguration.SERVICE_ENDPOINT_NAME, 
					PersistenceCouchBase.class);
			SecurityTokenProvider.instance.set(TestUtility.TOKEN);
		}
		
		long startTime = Calendar.getInstance().getTimeInMillis();
		long endTime = startTime;
		while(endTime <=  (startTime + 10*1000)){ // 10 sec
			endTime = Calendar.getInstance().getTimeInMillis();
		}
		
		logger.debug("Going to check First Time");
		PersistenceBackend first = PersistenceBackendFactory.getPersistenceBackend(TestUtility.getScope());
		logger.debug("First {} : {}", PersistenceBackend.class.getSimpleName(), first);
		
		publishResource(serviceEndpoint);
		
		startTime = Calendar.getInstance().getTimeInMillis();
		endTime = startTime;
		while(endTime <=  (startTime + (PersistenceBackendFactory.FALLBACK_RETRY_TIME + 100))){
			endTime = Calendar.getInstance().getTimeInMillis();
		}

		logger.debug("Going to check Second Time");
		PersistenceBackend second = PersistenceBackendFactory.getPersistenceBackend(TestUtility.getScope());
		logger.debug("Second {} : {}", PersistenceBackend.class.getSimpleName(), second);
		
		Assert.assertNotEquals(first, second);
		
	}
}
