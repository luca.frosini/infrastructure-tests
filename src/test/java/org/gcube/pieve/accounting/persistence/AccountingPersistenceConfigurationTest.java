/**
 * 
 */
package org.gcube.pieve.accounting.persistence;

import java.io.StringWriter;
import java.net.URL;

import org.gcube.accounting.persistence.AccountingPersistenceConfiguration;
import org.gcube.common.resources.gcore.Resources;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.Profile;
import org.gcube.common.resources.gcore.ServiceEndpoint.Property;
import org.gcube.common.resources.gcore.ServiceEndpoint.Runtime;
import org.gcube.common.resources.gcore.common.Platform;
import org.gcube.common.resources.gcore.utils.Group;
import org.gcube.documentstore.persistence.PersistenceBackend;
import org.gcube.pieve.testutility.ScopedTest;
import org.gcube.pieve.testutility.TestUtility;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class AccountingPersistenceConfigurationTest extends ScopedTest {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountingPersistenceConfigurationTest.class);
	
	private static final String PROFILE_DESCRIPTION = "This ServiceEndpoint contains the parameter to connect to DB to persist log accounting";
	private static final String HOSTED_ON = "pc-frosini.isti.cnr.it";
	private static final String ENDPOINT = "http://localhost:5984";
	
	private static final String READY = "READY";
	
	private static final String PLATFORM_NAME = "Platform Name";
	private static final String TEST_VERSION = "1.0.0";
	private static final short[] VERSION_SLICES = new short[]{1,6,0,0};
	
	private static final String DESCRIPTION = "Persistence Configuration Test";
	
	private static final String FAKE_USERNAME = "fakeusername";
	private static final String FAKE_PASSWORD = "fakepassword";
	
	private static final String DB_NAME_PROPERTY_KEY = "dbName";
	private static final String DB_NAME_PROPERTY_VALUE = "accounting";
	
	private abstract class AccountingPersistenceFakeDB extends PersistenceBackend {
		
	}
	
	/**
	 * Create the Service Endpoint using information related to discovered 
	 * available plugins and their own discovered capabilities 
	 * @return the created {@link ServiceEndpoint}
	 */
	protected static ServiceEndpoint createServiceEndpoint(){
		logger.debug("Getting Available Plugins and their own supported capabilities");
		
		logger.debug("Creating ServiceEndpoint to publish on IS available plugins and their own supported capabilities");
		ServiceEndpoint serviceEndpoint = new ServiceEndpoint();
		Profile profile = serviceEndpoint.newProfile();
		profile.category(AccountingPersistenceConfiguration.SERVICE_ENDPOINT_CATEGORY);
		profile.name(AccountingPersistenceConfiguration.SERVICE_ENDPOINT_NAME);
		profile.version(TEST_VERSION);
		profile.description(PROFILE_DESCRIPTION);
		
		
		Platform platform = profile.newPlatform();
		platform.name(PLATFORM_NAME);

		platform.version(VERSION_SLICES[0]);
		platform.minorVersion(VERSION_SLICES[1]);
		platform.buildVersion(VERSION_SLICES[2]);
		platform.revisionVersion(VERSION_SLICES[3]);
		
		Runtime runtime = profile.newRuntime();
		runtime.hostedOn(HOSTED_ON);
		runtime.status(READY);
		
		Group<AccessPoint> accessPoints = profile.accessPoints();
		
		AccessPoint accessPointElement = new AccessPoint();
		accessPoints.add(accessPointElement);
		accessPointElement.description(DESCRIPTION);
		accessPointElement.credentials(FAKE_USERNAME, FAKE_PASSWORD);
		
		accessPointElement.address(ENDPOINT);
		accessPointElement.name(AccountingPersistenceFakeDB.class.getSimpleName());
		
		Group<Property> properties = accessPointElement.properties();
		
		Property dbName = new Property();
		dbName.nameAndValue(DB_NAME_PROPERTY_KEY, DB_NAME_PROPERTY_VALUE);
		dbName.encrypted(false);
		properties.add(dbName);
		
		StringWriter stringWriter = new StringWriter();
		Resources.marshal(serviceEndpoint, stringWriter);
		logger.debug("The created ServiceEndpoint profile is\n{}", stringWriter.toString());
		
		return serviceEndpoint;
	}
	
	public void testPersistenceConfigurationFromIS() throws Exception{
		boolean createResource = true;
		ServiceEndpoint serviceEndpoint = null;
		
		if(createResource){
			serviceEndpoint = createServiceEndpoint();
			TestUtility.publishResource(serviceEndpoint);
		}
		
		Thread.sleep(5000); // Waiting 5 sec
		
		try {
			AccountingPersistenceConfiguration persitenceConfiguration = new AccountingPersistenceConfiguration(AccountingPersistenceFakeDB.class);
			if(createResource){
				String uri = persitenceConfiguration.getProperty(AccountingPersistenceConfiguration.URL_PROPERTY_KEY);
				Assert.assertTrue(uri.compareTo(new URL(ENDPOINT).toString())==0);
				String username = persitenceConfiguration.getProperty(AccountingPersistenceConfiguration.USERNAME_PROPERTY_KEY);
				Assert.assertTrue(username.compareTo(FAKE_USERNAME)==0);
				String password = persitenceConfiguration.getProperty(AccountingPersistenceConfiguration.PASSWORD_PROPERTY_KEY);
				Assert.assertTrue(password.compareTo(FAKE_PASSWORD)==0);
				
				String dbName = persitenceConfiguration.getProperty(DB_NAME_PROPERTY_KEY);
				Assert.assertTrue(dbName.compareTo(DB_NAME_PROPERTY_VALUE)==0);
			}
		} finally {
			if(createResource){
				TestUtility.unPublishResource(serviceEndpoint);
			}
		}
		
	}
	
	public void getUsernamePasswordForScopes() throws Exception{
		logger.debug("START ======================================================");
		try {
			AccountingPersistenceConfiguration persitenceConfiguration = new AccountingPersistenceConfiguration(AccountingPersistenceFakeDB.class);
			String uri = persitenceConfiguration.getProperty(AccountingPersistenceConfiguration.URL_PROPERTY_KEY);
			String username = persitenceConfiguration.getProperty(AccountingPersistenceConfiguration.USERNAME_PROPERTY_KEY);
			String password = persitenceConfiguration.getProperty(AccountingPersistenceConfiguration.PASSWORD_PROPERTY_KEY);
			logger.debug("{} - {} - {} - {}", TestUtility.getScope(), uri, username, password);
		}catch(IndexOutOfBoundsException e){
			logger.debug("No AccountingPersistenceConfiguration : \n {} {} \n\n", e.getClass().getName(), e.getMessage());
		} catch(Exception e){
			logger.error("Error getting AccountingPersistenceConfiguration", e);
			throw e;
		} finally {
			logger.debug(" END  ======================================================");
		}
	}
	
}
