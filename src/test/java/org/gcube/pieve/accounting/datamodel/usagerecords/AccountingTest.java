/**
 * 
 */
package org.gcube.pieve.accounting.datamodel.usagerecords;

import java.util.concurrent.TimeUnit;

import org.gcube.accounting.datamodel.usagerecords.JobUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.PortletUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.ServiceUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.StorageUsageRecord;
import org.gcube.accounting.persistence.AccountingPersistence;
import org.gcube.accounting.persistence.AccountingPersistenceFactory;
import org.gcube.documentstore.exception.InvalidValueException;
import org.gcube.documentstore.exception.NotAggregatableRecordsExceptions;
import org.gcube.pieve.accounting.datamodel.usagerecords.TaskUsageRecord;
import org.gcube.pieve.testutility.TestUsageRecord;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class AccountingTest {
	
	private static Logger logger = LoggerFactory.getLogger(AccountingTest.class);
	
	private AccountingPersistence accountingPersistence;
	
	@Before
	public void before(){
		accountingPersistence = AccountingPersistenceFactory.getPersistence();
	}
	
	@After
	public void after(){
		try {
			accountingPersistence.flush(1000, TimeUnit.MILLISECONDS);
		} catch (Exception e) {
			logger.error("Error flushing Buffered Records", e);
		}
	}
	
	@Test
	public void accountingServiceUsageRecordStressTest() throws InvalidValueException, NotAggregatableRecordsExceptions {
		
		for(int i=0; i<2; i++){
			ServiceUsageRecord sur = TestUsageRecord.createTestServiceUsageRecord();
			sur.setScope(TestUsageRecord.TEST_SCOPE);
			accountingPersistence.account(sur);
		}

	}
	
	
	@Test
	public void accountingStorageUsageRecordStressTest() throws InvalidValueException, NotAggregatableRecordsExceptions {
		for(int i=0; i<1000; i++){
			StorageUsageRecord sur = org.gcube.pieve.testutility.TestUsageRecord.createTestStorageUsageRecord();
			sur.setScope(TestUsageRecord.TEST_SCOPE);
			accountingPersistence.account(sur);
		}
	}
	
	@Test
	public void accountingJobUsageRecordStressTest() throws InvalidValueException, NotAggregatableRecordsExceptions {
		for(int i=0; i<1000; i++){
			JobUsageRecord jur = TestUsageRecord.createTestJobUsageRecord();
			jur.setScope(TestUsageRecord.TEST_SCOPE);
			accountingPersistence.account(jur);
		}
	}
	
	@Test
	public void accountingPortletUsageRecordStressTest() throws InvalidValueException, NotAggregatableRecordsExceptions {
		for(int i=0; i<1000; i++){
			PortletUsageRecord pur = TestUsageRecord.createTestPortletUsageRecord();
			pur.setScope(TestUsageRecord.TEST_SCOPE);
			accountingPersistence.account(pur);
		}
	}
	
	
	@Test
	public void accountingTaskUsageRecordStressTest() throws InvalidValueException, NotAggregatableRecordsExceptions {
		for(int i=0; i<1000; i++){
			TaskUsageRecord tur = TestUsageRecord.createTestTaskUsageRecord();
			tur.setScope(TestUsageRecord.TEST_SCOPE);
			accountingPersistence.account(tur);
		}
	}

	
}
