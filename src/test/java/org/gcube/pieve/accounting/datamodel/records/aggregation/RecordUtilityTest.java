/**
 * 
 */
package org.gcube.pieve.accounting.datamodel.records.aggregation;

import java.util.ArrayList;
import java.util.List;

import org.gcube.accounting.datamodel.usagerecords.JobUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.PortletUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.ServiceUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.StorageUsageRecord;
import org.gcube.documentstore.records.AggregatedRecord;
import org.gcube.documentstore.records.Record;
import org.gcube.documentstore.records.RecordUtility;
import org.gcube.pieve.accounting.datamodel.usagerecords.TaskUsageRecord;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class RecordUtilityTest {

	private static Logger logger = LoggerFactory.getLogger(RecordUtilityTest.class);
	
	@Test
	public void recordUtilityTest() {
		List<Class<? extends Record>> recordClasses = new ArrayList<>();
		recordClasses.add(ServiceUsageRecord.class);
		recordClasses.add(StorageUsageRecord.class);
		recordClasses.add(JobUsageRecord.class);
		recordClasses.add(TaskUsageRecord.class);
		recordClasses.add(PortletUsageRecord.class);
		for(Class<? extends Record> recordClass : recordClasses){
			try {
				@SuppressWarnings("rawtypes")
				Class<? extends AggregatedRecord> aggregatedClass = RecordUtility.getAggregatedRecordClass(recordClass.getSimpleName());
				logger.error("Aggregated Record Class for {} is {}", 
						recordClass.getSimpleName(), 
						aggregatedClass.getName());
			} catch (ClassNotFoundException e) {
				logger.error("Error getting Aggregated Record Class for {}", 
						recordClass.getSimpleName(), e);
			}
		}
	}
	
}
