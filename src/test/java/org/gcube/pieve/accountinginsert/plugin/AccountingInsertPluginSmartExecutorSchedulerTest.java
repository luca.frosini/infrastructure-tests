/**
 * 
 */
package org.gcube.pieve.accountinginsert.plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.gcube.pieve.testutility.ScopedTest;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.client.plugins.ExecutorPlugin;
import org.gcube.vremanagement.executor.client.proxies.SmartExecutorProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccountingInsertPluginSmartExecutorSchedulerTest extends ScopedTest {

	private static Logger logger = LoggerFactory.getLogger(AccountingInsertPluginSmartExecutorSchedulerTest.class);

	private SmartExecutorProxy proxy;
	@Before
	public void before() throws Exception{
		super.before();
	
		proxy = ExecutorPlugin.getExecutorProxy("Accounting-Insert-Storage").build();		
		Assert.assertNotNull(proxy);

	}

	public UUID scheduleTest(Scheduling scheduling) throws Exception {

		Map<String, Object> inputs = new HashMap<String, Object>();
		logger.debug("Inputs : {}", inputs);
		
		inputs.put("dataServiceClass","content-management");
		inputs.put("dataServiceName","storage-manager");
		inputs.put("dataServiceId","identifier");
		inputs.put("uri","MongoDb");
		inputs.put("dataType","STORAGE");
	
		LaunchParameter parameter = new LaunchParameter("Accounting-Insert-Storage", inputs);
		parameter.setScheduling(scheduling);

		try {
			String uuidString = proxy.launch(parameter);

		
			return UUID.fromString(uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}

	}



	@Test
	public void launch() {
		Map<String, Object> inputs = new HashMap<String, Object>();
		LaunchParameter launchParameter = new LaunchParameter("Test", inputs);
		try {
			proxy.launch(launchParameter);
		} catch (Exception e) {
			logger.error("Error launching sheduled task", e);
			//throw e;
		}

	}
	@Test
	public void LaunchTest() throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		/*optional*/
		inputs.put("dataServiceClass","content-management");
		inputs.put("dataServiceName","storage-manager");
		inputs.put("dataServiceId","identifier");
		inputs.put("uri","MongoDb");
		inputs.put("dataType","STORAGE");
	
		LaunchParameter parameter = new LaunchParameter("Accounting-Insert-Storage", inputs);
		//parameter.setScheduling(scheduling);
		try {
			String uuidString = proxy.launch(parameter);
			logger.debug("Launched with UUID : {}", uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}

	}


	@Test
	public void cronExpPreviousMustBeTerminated() throws Exception {
		CronExpression cronExpression = new CronExpression("0 0 6 * * ?"); // every day at 2:00
		//Launched with UUID : df3ca103-dc0a-477d-b7fe-1445c6984e6d
		Scheduling scheduling = new Scheduling(cronExpression, true);
		scheduling.setGlobal(true);
		UUID uuid = scheduleTest(scheduling);

		logger.debug("Launched with UUID : {}", uuid);


	}



	@Test
	public void unSchedule() throws Exception {
	
		//proxy.unSchedule("fde48bd2-097d-4196-8a81-b9304534c76b", true);
		
	
		//
	}

	@Test
	public void stop() throws Exception {
		proxy.stop("Accouting-Aggregator-Plugin");
	}

}
