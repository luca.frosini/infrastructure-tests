/**
 * 
 */
package org.gcube.pieve.testutility;

//import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.junit.After;
import org.junit.Before;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class ScopedTest {

	@Before
	public void before() throws Exception{

//		SecurityTokenProvider.instance.set("36501a0d-a205-4bf1-87ad-4c7185faa0d6-98187548");
//		ScopeProvider.instance.set("/gcube/devNext");

//		SecurityTokenProvider.instance.set("3acdde42-6883-4564-b3ba-69f6486f6fe0-98187548");
//		ScopeProvider.instance.set("/gcube");
		
		
	/*	DA SCOMMENTARE E LANCIARE UNA VOLTA COMPLETATO GCUBE APPS*/
		SecurityTokenProvider.instance.set("73cc40ab-dfe9-41c6-afa5-abd75de32d3c-843339462");
		ScopeProvider.instance.set("/d4science.research-infrastructures.eu");
	
	}
	
	@After
	public void after() throws Exception{
		SecurityTokenProvider.instance.reset();
		ScopeProvider.instance.reset();
	}
	
}
