/**
 * 
 */
package org.gcube.pieve.aggregator.plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.gcube.pieve.testutility.ScopedTest;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.client.plugins.ExecutorPlugin;
import org.gcube.vremanagement.executor.client.proxies.SmartExecutorProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AggregatorAccountingPluginSmartExecutorSchedulerTest extends ScopedTest {

	private static Logger logger = LoggerFactory.getLogger(AggregatorAccountingPluginSmartExecutorSchedulerTest.class);

	private SmartExecutorProxy proxy;
	@Before
	public void before() throws Exception{
		super.before();
		
		//ScopeProvider.instance.reset(); // Comment this to run the test. this line has been added to avoid unwanted launch
		//SecurityTokenProvider.instance.set(TestUtility.TOKEN);
		//ScopeProvider.instance.set("/gcube/devNext");
		proxy = ExecutorPlugin.getExecutorProxy("Accouting-Aggregator-Plugin").build();		
		Assert.assertNotNull(proxy);

	}

	public UUID scheduleTest(Scheduling scheduling) throws Exception {

		Map<String, Object> inputs = new HashMap<String, Object>();
		logger.debug("Inputs : {}", inputs);
		inputs.put("type","DAILY");
		//period to be processed
		inputs.put("interval",3);
		//change to time
		inputs.put("startTime", 14);
		//specify bucket
		inputs.put("bucket","accounting_service");
		//current scope
		inputs.put("currentScope",false);
		//specify user for save to workspace
		inputs.put("user","alessandro.pieve");
		//specify a recovery 0 default recovery and aggregate, 1 only aggregate, 2 only recovery
		inputs.put("recovery",0);


		//optional if present interval is not considered and elaborate a specificy step
				//e.g if type is daily and set input.put("intervalStep",10), this plugin elaborate a 10 hour 
		//		inputs.put("intervalStep",24);
				//optional if exist and true no backup, but start elaborate immediately
//				inputs.put("backup",false);
//				inputs.put("typePersisted",1);
		LaunchParameter parameter = new LaunchParameter("Accouting-Aggregator-Plugin", inputs);
		parameter.setScheduling(scheduling);

		try {
			String uuidString = proxy.launch(parameter);

		
			return UUID.fromString(uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}

	}



	@Test
	public void launch() {
		Map<String, Object> inputs = new HashMap<String, Object>();
		LaunchParameter launchParameter = new LaunchParameter("Test", inputs);
		try {
			proxy.launch(launchParameter);
		} catch (Exception e) {
			logger.error("Error launching sheduled task", e);
			//throw e;
		}

	}
	@Test
	public void LaunchTest() throws Exception {
		
		Map<String, Object> inputs = new HashMap<String, Object>();
		logger.debug("Inputs : {}", inputs);

		//inputs.put("type","DAILY");
		inputs.put("type","MONTHLY");
		//period to be processed
		inputs.put("interval",1);
		//change to time
		//novembre 6
		//ottobre 7
		//settembre 8
		//agosto 9
		//luglio 10
		//giugno e' 11 
		
		
		inputs.put("startTime",7);
		//inputs.put("startTime",173);
		inputs.put("intervalStep",4);		
		
		
	
		//specify bucket
		inputs.put("bucket","accounting_service");
		//current scope
		inputs.put("currentScope",false);
		//specify user for save to workspace
		inputs.put("user","alessandro.pieve");
		//specify a recovery 0 default recovery and aggregate, 1 only aggregate, 2 only recovery
		inputs.put("recovery",0);
		//optional if present interval is not considered and elaborate a specificy step
		//e.g if type is daily and set input.put("intervalStep",10), this plugin elaborate a 10 hour 
		
		//optional if exist and true no backup, but start elaborate immediately
		//inputs.put("backup",false);
		//inputs.put("typePersisted",1);

		LaunchParameter parameter = new LaunchParameter("Accouting-Aggregator-Plugin", inputs);
		try {
			String uuidString = proxy.launch(parameter);
			logger.debug("Launched with UUID : {}", uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}

	}

	
	
	
	
	@Test
	public void LaunchTestAutomatic() throws Exception {
		
		Map<String, Object> inputs = new HashMap<String, Object>();
		logger.debug("Inputs : {}", inputs);

		inputs.put("type","DAILY");
		//period to be processed
		inputs.put("interval",1);
		//change to time
		//load a file for start time
		inputs.put("pathFile","/home/gcube/SmartGears/startTime");
		
		//specify bucket
		inputs.put("bucket","accounting_service");
		inputs.put("endScriptTime","18:30");
		//current scope
		inputs.put("currentScope",false);
		//specify user for save to workspace
		inputs.put("user","alessandro.pieve");
		//specify a recovery 0 default recovery and aggregate, 1 only aggregate, 2 only recovery
		inputs.put("recovery",0);

		LaunchParameter parameter = new LaunchParameter("Accouting-Aggregator-Plugin", inputs);
		try {
			String uuidString = proxy.launch(parameter);
			logger.debug("Launched with UUID : {}", uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}

	}

	
	
	
	

	@Test
	public void cronExpPreviousMustBeTerminated() throws Exception {
		CronExpression cronExpression = new CronExpression("0 0 2 * * ?"); // every day at 2:00
		Scheduling scheduling = new Scheduling(cronExpression, true);
		scheduling.setGlobal(true);
		UUID uuid = scheduleTest(scheduling);
		logger.debug("Launched with UUID : {}", uuid);
	}



	@Test
	public void unSchedule() throws Exception {
		//proxy.unSchedule("", true); 
	
	}

	@Test
	public void stop() throws Exception {
		proxy.stop("Accouting-Aggregator-Plugin");
	}

}
