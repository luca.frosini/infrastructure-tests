/**
 * 
 */
package org.gcube.pieve.aggregator.plugin;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.client.plugins.ExecutorPlugin;
import org.gcube.vremanagement.executor.client.proxies.SmartExecutorProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AggregatorAccountingPluginSmartExecutorSchedulerTestProduction  {

	private static Logger logger = LoggerFactory.getLogger(AggregatorAccountingPluginSmartExecutorSchedulerTestProduction.class);

	private SmartExecutorProxy proxy;
	@Before
	public void before() throws Exception{
		String token="73cc40ab-dfe9-41c6-afa5-abd75de32d3c-843339462";
		SecurityTokenProvider.instance.set(token);
		ScopeProvider.instance.set("/d4science.research-infrastructures.eu");
		proxy = ExecutorPlugin.getExecutorProxy("Accouting-Aggregator-Plugin").build();		
		Assert.assertNotNull(proxy);

	}

	public UUID scheduleTest(Scheduling scheduling) throws Exception {

		Map<String, Object> inputs = new HashMap<String, Object>();
		logger.debug("Inputs : {}", inputs);
		inputs.put("type","DAILY");
		//period to be processed
		inputs.put("interval",1);
		
		inputs.put("pathFile","/home/gcube/SmartGears/startTime");
		//specify bucket
		inputs.put("bucket","accounting_service");
		inputs.put("endScriptTime","18:30");
		
		//specify bucket
		inputs.put("bucket","accounting_service");
		//current scope
		inputs.put("currentScope",false);
		//specify user for save to workspace
		inputs.put("user","alessandro.pieve");
		//specify a recovery 0 default recovery and aggregate, 1 only aggregate, 2 only recovery
		inputs.put("recovery",0);


		LaunchParameter parameter = new LaunchParameter("Accouting-Aggregator-Plugin", inputs);
		parameter.setScheduling(scheduling);

		try {
			String uuidString = proxy.launch(parameter);

			return UUID.fromString(uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}

	}


	
	
	@Test
	public void LaunchTest() throws Exception {
		
		Map<String, Object> inputs = new HashMap<String, Object>();
		logger.debug("Inputs : {}", inputs);

		//inputs.put("type","DAILY");
		inputs.put("type","MONTHLY");
		//period to be processed
		inputs.put("interval",1);
		//change to time
		//novembre 6
		//ottobre 7
		//settembre 8
		//agosto 9
		//luglio 10
		//giugno e' 11 
		
		
		
		inputs.put("startTime",8);
		//inputs.put("startTime",173);
		inputs.put("intervalStep",4);		
		
		
	
		//specify bucket
		inputs.put("bucket","accounting_service");
		//current scope
		inputs.put("currentScope",false);
		//specify user for save to workspace
		inputs.put("user","alessandro.pieve");
		//specify a recovery 0 default recovery and aggregate, 1 only aggregate, 2 only recovery
		inputs.put("recovery",0);
		//optional if present interval is not considered and elaborate a specificy step
		//e.g if type is daily and set input.put("intervalStep",10), this plugin elaborate a 10 hour 
		
		//optional if exist and true no backup, but start elaborate immediately
		//inputs.put("backup",false);
		//inputs.put("typePersisted",1);

		LaunchParameter parameter = new LaunchParameter("Accouting-Aggregator-Plugin", inputs);
		try {
			String uuidString = proxy.launch(parameter);
			logger.debug("Launched with UUID : {}", uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}

	}
	
	
	
	@Test
	public void cronExpPreviousMustBeTerminated() throws Exception {
		CronExpression cronExpression = new CronExpression("0 0 7 * * ?"); // every day at 2:00
		Scheduling scheduling = new Scheduling(cronExpression, true);
		scheduling.setGlobal(true);
		UUID uuid = scheduleTest(scheduling);

		logger.debug("Launched with UUID : {}", uuid);


	}



	@Test
	public void unSchedule() throws Exception {
		proxy.unSchedule("41909530-0142-4073-96e1-11c5d688994a", true); //ore 7
		
	}

	@Test
	public void stop() throws Exception {
		proxy.stop("96e9d82c-7fa8-4dda-a4fd-85696bb0575a");
	}

}
