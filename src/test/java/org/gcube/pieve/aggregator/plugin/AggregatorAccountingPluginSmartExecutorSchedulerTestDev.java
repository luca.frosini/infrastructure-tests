/**
 * 
 */
package org.gcube.pieve.aggregator.plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.client.plugins.ExecutorPlugin;
import org.gcube.vremanagement.executor.client.proxies.SmartExecutorProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AggregatorAccountingPluginSmartExecutorSchedulerTestDev  {

	private static Logger logger = LoggerFactory.getLogger(AggregatorAccountingPluginSmartExecutorSchedulerTestDev.class);

	private SmartExecutorProxy proxy;
	@Before
	public void before() throws Exception{
		SecurityTokenProvider.instance.set("36501a0d-a205-4bf1-87ad-4c7185faa0d6-98187548");
		//FOR DEBUG
		String scopeDebug="/gcube/devNext";
		ScopeProvider.instance.set(scopeDebug);
		proxy = ExecutorPlugin.getExecutorProxy("Accouting-Aggregator-Plugin").build();		
		Assert.assertNotNull(proxy);

	}

	public UUID scheduleTest(Scheduling scheduling) throws Exception {

		Map<String, Object> inputs = new HashMap<String, Object>();
		logger.debug("Inputs : {}", inputs);
		inputs.put("type","DAILY");
		//period to be processed
		inputs.put("interval",3);
		//change to time
		inputs.put("startTime", 14);
		//specify bucket
		inputs.put("bucket","accounting_service");
		//current scope
		inputs.put("currentScope",false);
		//specify user for save to workspace
		inputs.put("user","alessandro.pieve");
		//specify a recovery 0 default recovery and aggregate, 1 only aggregate, 2 only recovery
		inputs.put("recovery",0);

		LaunchParameter parameter = new LaunchParameter("Accouting-Aggregator-Plugin", inputs);
		parameter.setScheduling(scheduling);

		try {
			String uuidString = proxy.launch(parameter);

			return UUID.fromString(uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}

	}


	
	

	@Test
	public void cronExpPreviousMustBeTerminated() throws Exception {
		CronExpression cronExpression = new CronExpression("0 0 2 * * ?"); // every day at 2:00
		Scheduling scheduling = new Scheduling(cronExpression, true);
		scheduling.setGlobal(true);
		UUID uuid = scheduleTest(scheduling);
		logger.debug("Launched with UUID : {}", uuid);
	}



	@Test
	public void unSchedule() throws Exception {
		proxy.unSchedule("d588bb3f-a4c0-496c-b2ce-7c27059b44b7", true); //ore 2
		
	}

	@Test
	public void stop() throws Exception {
		proxy.stop("Accouting-Aggregator-Plugin");
	}

}
