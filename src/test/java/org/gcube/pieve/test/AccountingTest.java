/**
 * 
 */
package org.gcube.pieve.test;

import org.gcube.accounting.persistence.AccountingPersistence;
import org.gcube.accounting.persistence.AccountingPersistenceFactory;
import org.gcube.pieve.testutility.ScopedTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class AccountingTest extends ScopedTest {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountingTest.class);
	
	protected AccountingPersistence accountingPersistence;
	
	//@Before
	public void before() throws Exception {
		super.before();
		AccountingPersistenceFactory.setFallbackLocation("src/test/resources");
		accountingPersistence = AccountingPersistenceFactory.getPersistence();
	}
	
	//@Test
	public void testAccounting() throws Exception {
		logger.trace("Let See");
		Thread.sleep(1000*60*3);
		logger.trace("Finished");
	}
	
}
