/**
 * 
 */
package org.gcube.pieve.test;

import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.Resources;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.informationsystem.publisher.RegistryPublisherFactory;
import org.gcube.informationsystem.publisher.ScopedPublisher;
import org.gcube.informationsystem.publisher.exception.RegistryNotFoundException;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR) http://www.lucafrosini.com/
 *
 */
public class TempTest {

	private static final Logger logger = LoggerFactory.getLogger(TempTest.class);
	
	@Test
	public void test(){
		try {
			Class.forName("com.duke.MyLocaleServiceProvider");
			logger.debug("OK");
		} catch (ClassNotFoundException e) {
			logger.error("Not found", e);
		}
	}
	
	private static void publishScopedResource(Resource resource, List<String> scopes) throws RegistryNotFoundException, Exception {
		StringWriter stringWriter = new StringWriter();
		Resources.marshal(resource, stringWriter);
		
		ScopedPublisher scopedPublisher = RegistryPublisherFactory.scopedPublisher();
	    try {
			logger.debug("Trying to publish to {}:\n{}", scopes, stringWriter);
	    	scopedPublisher.create(resource, scopes);
		} catch (RegistryNotFoundException e) {
			logger.error("The resource was not published", e);
			throw e;
		}
	}
	
	@Test
	public void anotherTest(){
		ScopeProvider.instance.set("/gcube");
		try {
			
			GenericResource a = new GenericResource();
			 
			List<String> b = new LinkedList<>();
			String scope = ScopeProvider.instance.get();
			b.add(scope);
	 
			a.newProfile().name(UUID.randomUUID().toString()).type("FHN-nodes").description("FHN node");
			a.profile().newBody("<name>Nunzio</name>");
	 		
			try {
				publishScopedResource(a,b);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			logger.debug("OK");
		} catch (Exception e) {
			logger.error("Not found", e);
		}
	}
}
