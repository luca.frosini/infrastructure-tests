/**
 * 
 */
package org.gcube.pieve.accountinginsertRstudio.plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.gcube.pieve.testutility.ScopedTest;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.client.plugins.ExecutorPlugin;
import org.gcube.vremanagement.executor.client.proxies.SmartExecutorProxy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccountingInsertRstudioPluginSmartExecutorSchedulerTest extends ScopedTest {

	private static Logger logger = LoggerFactory.getLogger(AccountingInsertRstudioPluginSmartExecutorSchedulerTest.class);

	private SmartExecutorProxy proxy;
	@Before
	public void before() throws Exception{
		super.before();
		//ScopeProvider.instance.reset(); // Comment this to run the test. this line has been added to avoid unwanted launch
		//SecurityTokenProvider.instance.set(TestUtility.TOKEN);
		//ScopeProvider.instance.set("/gcube/devNext");
		proxy = ExecutorPlugin.getExecutorProxy("Accounting-Insert-RStudio").build();		
		Assert.assertNotNull(proxy);

	}

	public UUID scheduleTest(Scheduling scheduling) throws Exception {

		Map<String, Object> inputs = new HashMap<String, Object>();
		logger.debug("Inputs : {}", inputs);
		inputs.put("dataServiceClass","content-management");
		inputs.put("dataServiceName","storage-manager");		
		inputs.put("uri","Rstudio");
		inputs.put("dataType","STORAGE");
		inputs.put("unitVolume","Kilobyte");
		inputs.put("pathFile","/srv/d4science/home_disk_space");
				
		LaunchParameter parameter = new LaunchParameter("Accounting-Insert-RStudio", inputs);
		parameter.setScheduling(scheduling);

		try {
			String uuidString = proxy.launch(parameter);
			return UUID.fromString(uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}

	}



	@Test
	public void launch() {
		Map<String, Object> inputs = new HashMap<String, Object>();
		LaunchParameter launchParameter = new LaunchParameter("Test", inputs);
		try {
			proxy.launch(launchParameter);
		} catch (Exception e) {
			logger.error("Error launching sheduled task", e);
			//throw e;
		}

	}
	@Test
	public void LaunchTest() throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		inputs.put("dataServiceClass","content-management");
		inputs.put("dataServiceName","storage-manager");		
		inputs.put("uri","Rstudio");
		inputs.put("dataType","STORAGE");
		inputs.put("unitVolume","Kilobyte");
		inputs.put("pathFile","/srv/d4science/home_disk_space");
		LaunchParameter parameter = new LaunchParameter("Accounting-Insert-RStudio", inputs);
		//parameter.setScheduling(scheduling);
		try {
			String uuidString = proxy.launch(parameter);
			logger.debug("Launched with UUID : {}", uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}

	}


	@Test
	public void cronExpPreviousMustBeTerminated() throws Exception {
		CronExpression cronExpression = new CronExpression("0 0 16 * * ?"); // every day at 2:00
		//Launched with UUID : df3ca103-dc0a-477d-b7fe-1445c6984e6d
		Scheduling scheduling = new Scheduling(cronExpression, true);
		scheduling.setGlobal(true);
		UUID uuid = scheduleTest(scheduling);

		logger.debug("Launched with UUID : {}", uuid);


	}

	@Test
	public void unSchedule() throws Exception {
		//56e33a1f-ab24-4d7b-a7b7-cac29147136e active on prod at 18 o clock
		
		//active for each day on 23 cf5f3f75-4e1e-4d8c-ac87-6bfd20ec7330
		//active for each day on 23 0f84c6f9-3f43-40c6-8a8d-00cf2a3b311d
		//proxy.unSchedule("542ddb03-d8d7-4913-8700-2acfa74c7485", true);
		
	
		//
	}

	@Test
	public void stop() throws Exception {
		proxy.stop("Accouting-Insert-RStudio");
	}

}
