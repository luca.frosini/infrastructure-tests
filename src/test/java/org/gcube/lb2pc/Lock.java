package org.gcube.lb2pc;

public class Lock {
	
	protected String lockURI;
	
	protected LockType type;
	protected String transactionURI;
	protected String resourceURI;
	
	protected String contentType;
	
	public Lock(LockType type, String transactionURI, String resourceURI) {
		super();
		this.type = type;
		this.transactionURI = transactionURI;
		this.resourceURI = resourceURI;
	}
	
	public Lock(String lockURI) {
		this.lockURI = lockURI;
	}
	
	public LockType getType() {
		return type;
	}
	
	public void setType(LockType type) {
		this.type = type;
	}
	
	public String getTransactionURI() {
		return transactionURI;
	}
	
	public void setTransactionURI(String transactionURI) {
		this.transactionURI = transactionURI;
	}
	
	public String getResourceURI() {
		return resourceURI;
	}
	
	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}
	
	public String getLockURI() {
		return lockURI;
	}
	
	public String getContentType() {
		return contentType;
	}
	
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	public void setLockURI(String lockURI) {
		this.lockURI = lockURI;
	}
	
	public void create() {
		
	}
	
	public void read(String contentType) {
		this.contentType = contentType;
		
		HttpResponse lockResponse = Proxy.createHttpRequest(HttpMethod.GET, contentType, lockURI);
		if(lockResponse.getHttpStatusCode() != HttpStatusCode._200) {
			// Lock Not Found or any error which does not allow to proceed
			Proxy.sendErrorResponseToClient(HttpStatusCode._401, "Provided Lock does not exist");
		}
		
		unmarshalContent(lockResponse.getContent(), contentType);
	}
	
	private void unmarshalContent(String content, String contentType) {
		// Set fields
	}
	
	public void upgrade() {
		if(type == LockType.X) {
			return;
		}
		
		type = LockType.S;
		
		// HttpResponse lockResponse = Proxy.createHttpRequest(HttpMethod.PUT, contentType, lockURI);
		
		
	}
	
	public void delete() {
		// HttpResponse lockResponse = Proxy.createHttpRequest(HttpMethod.DELETE, null, lockURI);
		
	}
}
