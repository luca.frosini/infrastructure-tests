package org.gcube.lb2pc;

public enum HttpStatusCode {
	
	// 2×× Success
	_200(200, "OK"), _201(201, "Created"), _204(204, "No Content"),
	
	// 4×× Client Error
	_400(400, "Bad Request"), _401(401, "Unauthorized"), _403(403, "Forbidden"), _404(404, "Not Found"), 
	_405(405, "Method Not Allowed"), _406(406, "Not Acceptable"), _408(408, "Request Timeout"),
	_423(423, "Locked"),
	
	// 5×× Server Error
	_500(500, "Internal Server Error"), _503(503, "Service Unavailable");
	
	private int code;
	private String messageCode;
	
	private HttpStatusCode(int code, String messageCode) {
		this.code = code;
		this.messageCode = messageCode;
	}
	
	public int getCode() {
		return code;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	
}