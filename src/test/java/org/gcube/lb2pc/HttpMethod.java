package org.gcube.lb2pc;

public enum HttpMethod {
	POST, GET, PUT, DELETE, OPTIONS, HEAD, TRACE, CONNECT
}