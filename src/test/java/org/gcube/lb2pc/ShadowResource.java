package org.gcube.lb2pc;

import java.net.MalformedURLException;
import java.net.URL;

public class ShadowResource {
	
	protected String transactionURI;
	protected String requestURI;
	protected String lockURI;
	protected String contentType;
	protected String content;
	
	public ShadowResource(String transactionURI, String requestURI, String lockURI, String contentType) {
		super();
		this.transactionURI = transactionURI;
		this.requestURI = requestURI;
		this.lockURI = lockURI;
		this.contentType = contentType;
	}

	public ShadowResource(String transactionURI, String requestURI, String lockURI, String contentType,
			String content) {
		super();
		this.transactionURI = transactionURI;
		this.requestURI = requestURI;
		this.lockURI = lockURI;
		this.contentType = contentType;
		this.content = content;
	}

	public String getTransactionURI() {
		return transactionURI;
	}

	public void setTransactionURI(String transactionURI) {
		this.transactionURI = transactionURI;
	}

	public String getRequestURI() {
		return requestURI;
	}

	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}

	public String getLockURI() {
		return lockURI;
	}

	public void setLockURI(String lockURI) {
		this.lockURI = lockURI;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public void create() throws MalformedURLException {
		String shadowResourceURI = getURI();
		HttpResponse response = Proxy.createHttpRequest(HttpMethod.PUT, contentType, marshall(), shadowResourceURI);
		if(response.getHttpStatusCode() != HttpStatusCode._201) {
			Proxy.sendErrorResponseToClient(HttpStatusCode._403, "Unable to fullfill request");
		}
	}
	
	protected String marshall() {
		return null;
	}
	
	protected String getURI() throws MalformedURLException {
		return getShadowResourceURI(transactionURI, requestURI);
	}
	
	protected static String getPath(String uri) throws MalformedURLException {
		URL url = new URL(uri);
		return url.getPath();
	}
	
	public static String getShadowResourceURI(String transactionURI, String resourceURI) throws MalformedURLException {
		String path = getPath(resourceURI);
		
		if(path.contains("/operations/")) {
			path.replace("/operations/", "/_operations/");
		}
		
		if(path.endsWith("/operations")) {
			path.replace("/operations", "/_operations");
		}
		
		if(transactionURI.endsWith("/")) {
			transactionURI = transactionURI.substring(0, transactionURI.length() - 2);
		}
		
		return transactionURI + path;
	}
}
