package org.gcube.lb2pc;

import java.net.MalformedURLException;
import java.net.URL;

public class Log {
	
	protected HttpMethod operation;
	protected String resource;
	protected URL locationURI;
	
	protected String contentType;
	protected String shadowResourceURI;
	
	public Log(HttpMethod httpMethod) {
		super();
		this.operation = httpMethod;
		this.resource = null;
	}
	
	public Log(HttpMethod httpMethod, String resource, String locationURI) throws MalformedURLException {
		super();
		this.operation = httpMethod;
		this.resource = resource;
		this.locationURI = new URL(locationURI);
	}
	
	public HttpMethod getOperation() {
		return operation;
	}

	public void setOperation(HttpMethod operation) {
		this.operation = operation;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getShadowResourceURI() {
		return shadowResourceURI;
	}

	public void setShadowResourceURI(String shadowResourceURI) {
		this.shadowResourceURI = shadowResourceURI;
	}

	public void create(String shadowResourceURI, String contentType) {
		this.contentType = contentType;
		this.shadowResourceURI = shadowResourceURI;
		
		HttpResponse logResponse = Proxy.createHttpRequest(HttpMethod.POST, contentType, getLogCollectionURI());
		if(logResponse.getHttpStatusCode() != HttpStatusCode._201) {
			// Unable to create log. This does not allow to proceed
			Proxy.sendErrorResponseToClient(HttpStatusCode._401, "Provided Lock does not exist");
		}
		
		unmarshalContent(this, logResponse.getContent(), contentType);
	}
	
	protected String getLogCollectionURI() {
		return shadowResourceURI + "/operations";
	}
	
	public static void unmarshalContent(Log operationLog, String content, String contentType) {
		// Set fields
	}
	
}
