package org.gcube.authorization;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import org.gcube.common.authorization.client.Binder;
import org.gcube.common.authorization.client.Constants;
import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.ContainerInfo;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.library.provider.UserInfo;
import org.gcube.testutility.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextAuthorizationTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(ContextAuthorizationTest.class);
	
	@Test
	public void getInfo() throws Exception {
		ContextTest.setContext("");
	}
	
	@Test
	public void generateApplicationToken() throws Exception {
		ContextTest.setContextByName("/d4science.research-infrastructures.eu");
		//ContextTest.setContextByName("/pred4s");
		// ContextTest.setContextByName("/gcube");
		
		String context = "/d4science.research-infrastructures.eu/ParthenosVO/PARTHENOS_Registry";
		//String context = "/pred4s/preprod/preVRE";
		// String context = "/gcube/devNext/NextNext";
		
		UserInfo userInfo = new UserInfo("luca.frosini",new ArrayList<>());
		String userToken = authorizationService().generateUserToken(userInfo, context);
		SecurityTokenProvider.instance.set(userToken);
		
		String applicationName = "gCat"; 
		String generatedToken = authorizationService().generateExternalServiceToken(applicationName);
		
		logger.trace("Application Token for application {} in Context {} is {}", applicationName,
				ContextUtility.getCurrentContext(), generatedToken);
		
	}
	
	@Test
	public void generateHostToken() throws Exception {
		ContextTest.setContextByName("/d4science.research-infrastructures.eu");
		String currentContext = "/d4science.research-infrastructures.eu/ParthenosVO/PARTHENOS_Registry"; //ContextUtility.getCurrentContext();
		
		String host = "gcat.d4science.org";
		int port = 80;
		
		ContainerInfo containerInfo = new ContainerInfo(host, port);
		String tokenNode = authorizationService().requestActivation(containerInfo, currentContext);
		
		logger.trace("Token for Context {} for host {}:{} is {}", currentContext, host, port, tokenNode);
		
	}
	
	
	
	@Test
	public void generateUserToken() throws Exception {
		String currentContext = ContextUtility.getCurrentContext();
		AuthorizationEntry authorizationEntry = Constants.authorizationService().get(SecurityTokenProvider.instance.get());
		UserInfo userInfo = (UserInfo) authorizationEntry.getClientInfo();
		String tokenNode = authorizationService().generateUserToken(userInfo, currentContext);
		logger.trace("Token for Context {} for {} is {}", currentContext, userInfo.getId(), tokenNode);
	}
	
	
	@Test
	public void seriliazeUser() throws Exception {
		OutputStream output = new OutputStream()
	    {
	        private StringBuilder string = new StringBuilder();
	        @Override
	        public void write(int b) throws IOException {
	            this.string.append((char) b );
	        }

	        //Netbeans IDE automatically overrides this toString()
	        public String toString(){
	            return this.string.toString();
	        }
	    };
		
		UserInfo userInfo = new UserInfo("luca.frosini", new ArrayList<>());
		Binder.getContext().createMarshaller().marshal(userInfo, output);
		
		
		logger.debug("{}", output.toString());
	}
}
