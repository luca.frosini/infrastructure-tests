package org.gcube.phd;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.gcube.common.scope.impl.ScopeBean;
import org.gcube.common.scope.impl.ScopeBean.Type;
import org.gcube.context.ContextElaborator;

public class StatisticsCollector extends ContextElaborator {
	
	private Map<Type,Integer> contextCount;
	private SortedMap<ScopeBean, ContextStatistic> sortedMap; 
	
	public StatisticsCollector() {}
	
	@Override
	public void elaborateContext(ScopeBean scopeBean) throws Exception {
		Type type = scopeBean.type();
		Integer integer = contextCount.get(type);
		if(integer == null) {
			integer = 0;
		}
		contextCount.put(type, integer + 1);
		
		ContextStatistic contextStatistic = new ContextStatistic(scopeBean);
		sortedMap.put(scopeBean, contextStatistic);
	}
	
	public SortedMap<ScopeBean, ContextStatistic> getStatistics() throws Exception{
		if(sortedMap == null) {
			contextCount = new HashMap<>();
			sortedMap = new TreeMap<>();
			all();
		}
		return sortedMap;
	}
	
	public Map<Type,Integer> getContextCount() throws Exception {
		if(contextCount == null) {
			contextCount = new HashMap<>();
			sortedMap = new TreeMap<>(new ScopeBeanComparator());
			all();
		}
		return contextCount;
	}
	
}
