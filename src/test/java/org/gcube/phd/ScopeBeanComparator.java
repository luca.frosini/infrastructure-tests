package org.gcube.phd;

import java.util.Comparator;

import org.gcube.common.scope.impl.ScopeBean;

public class ScopeBeanComparator implements Comparator<ScopeBean> {
	
	@Override
	public int compare(ScopeBean scopeBean1, ScopeBean scopeBean2) {
		return scopeBean1.toString().compareTo(scopeBean2.toString());
	}
}
