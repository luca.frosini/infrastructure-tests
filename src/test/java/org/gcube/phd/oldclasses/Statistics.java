package org.gcube.phd.oldclasses;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.HostingNode;
import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.Resources;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.Software;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClient;
import org.gcube.informationsystem.resourceregistry.client.ResourceRegistryClientFactory;
import org.gcube.informationsystem.utils.ISMapper;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.icclient.ICFactory;
import org.gcube.testutility.ContextTest;
import org.junit.Test;

public class Statistics extends ContextTest {
	
	private File directory;
	
	
	private void printLineToFile(String line, File file) throws IOException {
		synchronized (file) {
			try (FileWriter fw = new FileWriter(file, true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter out = new PrintWriter(bw)) {
				out.println(line);
				out.flush();
			} catch (IOException e) {
				throw e;
			}
		}
	}
	
	protected String getUnmarshalledResource(Resource gr) {
		StringWriter stringWriter = new StringWriter();
		Resources.marshal(gr, stringWriter);
		return stringWriter.toString();
	}
	
	private void addToCSV(UUID uuid, Resource gr, org.gcube.informationsystem.model.reference.entities.Resource r, File statisticsFile, boolean addHeader) throws IOException {
		StringBuffer stringBuffer = new StringBuffer();
		if(addHeader) {
			stringBuffer.append(gr.getClass().getSimpleName());
			stringBuffer.append("(byte size)");
			stringBuffer.append(",");
			stringBuffer.append(r.getClass().getSimpleName());
			stringBuffer.append("(byte size)");
			printLineToFile(stringBuffer.toString(), statisticsFile);
			stringBuffer = new StringBuffer();
		}
		String unmarshalledGR = getUnmarshalledResource(gr);
		File xmlFile = new File(directory, uuid.toString()+".xml");
		if(xmlFile.exists()) {
			xmlFile.delete();
		}
		printLineToFile(unmarshalledGR, xmlFile);
		
		final byte[] grUTF8Bytes = unmarshalledGR.getBytes("UTF-8");
		stringBuffer.append(grUTF8Bytes.length);
		stringBuffer.append(",");
		String unmarshalledR = ISMapper.marshal(r);
		File jsonFile = new File(directory, uuid.toString()+".json");
		if(jsonFile.exists()) {
			jsonFile.delete();
		}
		printLineToFile(unmarshalledR, jsonFile);
		
		final byte[] rUTF8Bytes = unmarshalledR.getBytes("UTF-8");
		stringBuffer.append(rUTF8Bytes.length);
		printLineToFile(stringBuffer.toString(), statisticsFile);
	}
	
	private void addToCSV(Resource r, File statisticsFile, boolean addHeader) throws IOException {
		StringBuffer stringBuffer = new StringBuffer();
		if(addHeader) {
			stringBuffer.append(r.getClass().getSimpleName());
			stringBuffer.append("(byte size)");
			printLineToFile(stringBuffer.toString(), statisticsFile);
			stringBuffer = new StringBuffer();
		}
		String unmarshalledR = getUnmarshalledResource(r);
		final byte[] grUTF8Bytes = unmarshalledR.getBytes("UTF-8");
		stringBuffer.append(grUTF8Bytes.length);
		printLineToFile(stringBuffer.toString(), statisticsFile);
	}
	
	@Test
	public void test() throws Exception {
		ContextTest.setContextByName("/gcube");
		
		boolean first = true;
		
		directory =  new File("/home/lucafrosini/Dropbox/Dottorato/Review Tesi");
		
		File eServicesFile = new File(directory, "EService.csv");
		if(eServicesFile.exists()) {
			eServicesFile.delete();
		}
		File hostingNodeFile = new File(directory, "HostingNode.csv");
		if(hostingNodeFile.exists()) {
			hostingNodeFile.delete();
		}
		
		List<UUID> serviceIDs = new ArrayList<>();
		// Data Tansfer Service
		serviceIDs.add(UUID.fromString("d33619ab-4084-492f-800c-a2197a610132"));
		// Smart Executor Service
		serviceIDs.add(UUID.fromString("f7b0030a-4608-4560-bb6e-c1f33a7ad9f6"));
		// Web Hosting Node Service (WHNMAnager)
		serviceIDs.add(UUID.fromString("3aa75ba7-27d0-49a2-8ffc-356eb012d305"));
		DiscoveryClient<GCoreEndpoint> client = ICFactory.clientFor(GCoreEndpoint.class);
		
		ResourceRegistryClient resourceRegistryClient = ResourceRegistryClientFactory.create();
		
		for(UUID uuid : serviceIDs){
			SimpleQuery query = ICFactory.queryFor(GCoreEndpoint.class);
			query.addCondition(String.format("$resource/ID/text() eq '%1s'", uuid.toString()));
			List<GCoreEndpoint> gces = client.submit(query);
			GCoreEndpoint gCoreEndpoint = gces.get(0);
			
			EService eService = resourceRegistryClient.getInstance(EService.class, uuid);
			addToCSV(uuid, gCoreEndpoint, eService, eServicesFile, first);
			first= false;
		}
		
		
		
		
		UUID hostingNodeUUID = UUID.fromString("e81b39b6-0ee4-49cd-805f-ac133544b6fe");
		DiscoveryClient<HostingNode> hnClient = ICFactory.clientFor(HostingNode.class);
		SimpleQuery query = ICFactory.queryFor(HostingNode.class);
		query.addCondition(String.format("$resource/ID/text() eq '%1s'", hostingNodeUUID.toString()));
		List<HostingNode> hostingNodes = hnClient.submit(query);
		HostingNode hostingNode = hostingNodes.get(0);
		
		org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode hn = 
				resourceRegistryClient.getInstance(org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode.class, hostingNodeUUID);
		addToCSV(hostingNodeUUID, hostingNode, hn, hostingNodeFile, true);
		
		
	}
	
	public <R extends Resource> void analizeInstances(Class<R> clz) throws IOException {
		boolean first = true;
		
		File statisticsFile = new File(directory, clz.getSimpleName() + ".csv");
		if(statisticsFile.exists()) {
			statisticsFile.delete();
		}
				
		DiscoveryClient<R> client = ICFactory.clientFor(clz);
		SimpleQuery query = ICFactory.queryFor(clz);
		List<R> instances = client.submit(query);
		
		for(R r : instances){
			addToCSV(r, statisticsFile, first);
			first= false;
		}
	}
	
	@Test
	public void generateStatistics() throws Exception {
		// directory =  new File("/home/lucafrosini/Dropbox/Dottorato/Review Tesi/Statistiche/" + ScopedTest.getCurrentContext());
		directory =  new File("/home/lucafrosini/Desktop/Statistiche/" + ContextTest.getCurrentContext());
		if(!directory.exists()) {
			directory.mkdirs();
		}
		
		List<Class<? extends Resource>> classes = new ArrayList<>();
		classes.add(GCoreEndpoint.class);
		classes.add(HostingNode.class);
		classes.add(ServiceEndpoint.class);
		classes.add(GenericResource.class);
		classes.add(Software.class);
		
		for(Class<? extends Resource> clz : classes) {
			analizeInstances(clz);
		}
		
	}
	
	
	@Test
	public void generateStatisticsForContexts() throws Exception {
		List<String> contexts = new ArrayList<>();
		contexts.add("/d4science.research-infrastructures.eu");
		contexts.add("/d4science.research-infrastructures.eu/gCubeApps");
		contexts.add("/d4science.research-infrastructures.eu/SoBigData");
		contexts.add("/d4science.research-infrastructures.eu/ParthenosVO");
		
		for(String token : contexts) {
			ContextTest.setContextByName(token);
			generateStatistics();
		}
	}
	
}
