package org.gcube.phd.oldclasses;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.gcube.common.authorization.client.Constants;
import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.library.provider.UserInfo;
import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.HostingNode;
import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.Resources;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.Software;
import org.gcube.common.scope.impl.ScopeBean;
import org.gcube.context.ContextElaborator;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.icclient.ICFactory;
import org.gcube.testutility.ContextTest;
import org.junit.Test;

public class OLDGlobalStatistics extends ContextElaborator {
	
	private File directory;
	
	private Set<String> visited;
	
	public OLDGlobalStatistics() {
		visited = new HashSet<String>();
		directory =  new File("/home/lucafrosini/Desktop/Statistiche/Global");
		if(!directory.exists()) {
			directory.mkdirs();
		}
	}
	
	private void printLineToFile(String line, File file) throws IOException {
		synchronized (file) {
			try (FileWriter fw = new FileWriter(file, true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter out = new PrintWriter(bw)) {
				out.println(line);
				out.flush();
			} catch (IOException e) {
				throw e;
			}
		}
	}
	
	protected String getUnmarshalledResource(Resource gr) {
		StringWriter stringWriter = new StringWriter();
		Resources.marshal(gr, stringWriter);
		return stringWriter.toString();
	}
	
	private void addToCSV(Resource r) throws IOException {
		StringBuffer stringBuffer = new StringBuffer();
		File statisticsFile = new File(directory, r.getClass().getSimpleName() + ".csv");
		if(!statisticsFile.exists()) {
			statisticsFile.createNewFile();
			stringBuffer.append(r.getClass().getSimpleName());
			stringBuffer.append(" (byte)");
			printLineToFile(stringBuffer.toString(), statisticsFile);
			stringBuffer = new StringBuffer();
		}
		String unmarshalledR = getUnmarshalledResource(r);
		final byte[] grUTF8Bytes = unmarshalledR.getBytes("UTF-8");
		stringBuffer.append(grUTF8Bytes.length);
		printLineToFile(stringBuffer.toString(), statisticsFile);
	}
	
	public <R extends Resource> void analizeInstances(Class<R> clz) throws IOException {
		DiscoveryClient<R> client = ICFactory.clientFor(clz);
		SimpleQuery query = ICFactory.queryFor(clz);
		List<R> instances = client.submit(query);
		for(R r : instances){
			String id = r.id();
			if(!visited.contains(id)) {
				visited.add(id);
				addToCSV(r);
			}else {
				logger.debug("{} already managed", id);
			}
		}
	}
	
	public void generateStatistics() throws Exception {
		List<Class<? extends Resource>> classes = new ArrayList<>();
		classes.add(GCoreEndpoint.class);
		classes.add(HostingNode.class);
		classes.add(ServiceEndpoint.class);
		classes.add(GenericResource.class);
		classes.add(Software.class);
		
		for(Class<? extends Resource> clz : classes) {
			analizeInstances(clz);
		}
		
	}
	
	public String generateUserToken(String context) throws Exception {
		AuthorizationEntry authorizationEntry = Constants.authorizationService().get(SecurityTokenProvider.instance.get());
		UserInfo userInfo = (UserInfo) authorizationEntry.getClientInfo();
		String userToken = authorizationService().generateUserToken(userInfo, context);
		logger.trace("Token for Context {} for {} is {}", context, userInfo.getId(), userToken);
		return userToken;
	}
	
	@Override
	public void elaborateContext(ScopeBean scopeBean) throws Exception {
		String token = generateUserToken(scopeBean.toString());
		ContextTest.setContext(token);
		generateStatistics();
	}
	
	@Test
	public void test() throws Exception {
		ContextTest.setContextByName("/d4science.research-infrastructures.eu");
		all();
	}
	
}
