package org.gcube.phd;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.gcube.accounting.aggregator.utility.Utility;
import org.gcube.accounting.analytics.Filter;
import org.gcube.accounting.analytics.Info;
import org.gcube.accounting.analytics.TemporalConstraint;
import org.gcube.accounting.analytics.TemporalConstraint.AggregationMode;
import org.gcube.accounting.analytics.persistence.AccountingPersistenceQuery;
import org.gcube.accounting.analytics.persistence.AccountingPersistenceQueryFactory;
import org.gcube.accounting.datamodel.aggregation.AggregatedServiceUsageRecord;
import org.gcube.common.authorization.client.Constants;
import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.library.provider.UserInfo;
import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.HostingNode;
import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.Software;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.scope.impl.ScopeBean;
import org.gcube.documentstore.records.AggregatedRecord;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.icclient.ICFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContextStatistic {
	
	protected Logger logger = LoggerFactory.getLogger(ContextStatistic.class);
	
	public static Calendar START_TIME;
	public static Calendar END_TIME;
	private static AccountingPersistenceQuery apq;
	
	static {
		START_TIME = Utility.getAggregationStartCalendar(2017, Calendar.JANUARY, 1);
		END_TIME = Utility.getAggregationStartCalendar(2017, Calendar.DECEMBER, 1);
		apq = AccountingPersistenceQueryFactory.getInstance();
	}
	
	private ScopeBean scopeBean;
	
	public ScopeBean getScopeBean() {
		return scopeBean;
	}
	
	private Map<Class<? extends Resource>,Set<ResourceInfo<? extends Resource>>> resourceMap;
	private Map<Class<? extends Resource>,SummaryStatistics> statisticsMap;
	private SortedMap<Calendar, Info> accountingData;
	
	public ContextStatistic(ScopeBean scopeBean) {
		this.scopeBean = scopeBean;
	}
	
	public <R extends Resource> void analizeInstances(Class<R> clz) throws Exception {
		Set<ResourceInfo<? extends Resource>> resources = resourceMap.get(clz);
		if(resources==null) {
			resources = new TreeSet<>();
			resourceMap.put(clz, resources);
		}
		DiscoveryClient<R> client = ICFactory.clientFor(clz);
		SimpleQuery query = ICFactory.queryFor(clz);
		// logger.debug(query.expression());
		List<R> instances = client.submit(query);
		for(R r : instances) {
			ResourceInfo<R> resourceInfo = new ResourceInfo<R>(r);
			if(r instanceof HostingNode) {
				HostingNode hn = (HostingNode) r;
				if(hn.profile().description().status().compareTo("certified")!=0) {
					continue;
				}
				
				Calendar now = Calendar.getInstance();
				now.add(Calendar.DAY_OF_MONTH, -1);
				if(hn.profile().description().lastUpdate().before(now)) {
					continue;
				}
			}
			
			if(r instanceof GCoreEndpoint) {
				GCoreEndpoint gce = (GCoreEndpoint) r;
				if(gce.profile().deploymentData().status().compareTo("ready")!=0) {
					continue;
				}
			}
			resources.add(resourceInfo);
		}
	}
	
	@SuppressWarnings("unused")
	private String generateUserToken(String context) throws Exception {
		AuthorizationEntry authorizationEntry = Constants.authorizationService()
				.get(SecurityTokenProvider.instance.get());
		UserInfo userInfo = (UserInfo) authorizationEntry.getClientInfo();
		String userToken = authorizationService().generateUserToken(userInfo, context);
		logger.trace("Token for Context {} for {} is {}", context, userInfo.getId(), userToken);
		return userToken;
	}
	
	
	public Map<Class<? extends Resource>,Set<ResourceInfo<? extends Resource>>> getAllResources() throws Exception {
		if(resourceMap == null) {
			resourceMap = new HashMap<>();
			List<Class<? extends Resource>> classes = new ArrayList<>();
			classes.add(HostingNode.class);
			classes.add(GCoreEndpoint.class);
			classes.add(ServiceEndpoint.class);
			classes.add(GenericResource.class);
			classes.add(Software.class);
			
			String contextFullName = scopeBean.toString();
			/*
			String token = generateUserToken(contextFullName);
			ContextTest.setContext(token);
			*/
			ScopeProvider.instance.set(contextFullName);
			
			for(Class<? extends Resource> clz : classes) {
				Thread.sleep(TimeUnit.SECONDS.toMillis(5));
				analizeInstances(clz);
			}
		}
		return resourceMap;
	}
	
	public static Map<Class<? extends Resource>,SummaryStatistics> generateStatistics(
			Map<Class<? extends Resource>,Set<ResourceInfo<? extends Resource>>> resourceMap)
			throws UnsupportedEncodingException {
		Map<Class<? extends Resource>,SummaryStatistics> statisticsMap = new HashMap<>();
		for(Class<? extends Resource> clz : resourceMap.keySet()) {
			SummaryStatistics summaryStatistics = new SummaryStatistics();
			Set<ResourceInfo<? extends Resource>> resources = resourceMap.get(clz);
			for(ResourceInfo<? extends Resource> resourceInfo : resources) {
				summaryStatistics.addValue(resourceInfo.getSize());
			}
			statisticsMap.put(clz, summaryStatistics);
		}
		return statisticsMap;
	}
	
	public SortedMap<Calendar, Info> getAccountingData() throws Exception {
		if(accountingData==null) {
		
			TemporalConstraint temporalConstraint = new TemporalConstraint(
					START_TIME.getTimeInMillis(), END_TIME.getTimeInMillis(),
					AggregationMode.MONTHLY);
			List<Filter> filters = new ArrayList<>();
			Filter filter = new Filter("serviceClass", "InformationSystem");
			filters.add(filter);
			
			Class<? extends AggregatedRecord<?, ?>> clz = AggregatedServiceUsageRecord.class;
			
			// AccountingPersistenceQueryFactory.getForcedQueryScope().set(scopeBean.toString());
			
			accountingData = apq.getTimeSeries(clz, temporalConstraint,
					filters, true);
		}
		return accountingData;
	}
	
	public Map<Class<? extends Resource>,SummaryStatistics> getStatistics() throws Exception {
		if(statisticsMap == null) {
			getAllResources();
			statisticsMap = generateStatistics(resourceMap);
		}
		return statisticsMap;
	}
	
}
