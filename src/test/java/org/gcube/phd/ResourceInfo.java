package org.gcube.phd;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.Resources;

public class ResourceInfo<R extends Resource> implements Comparable<ResourceInfo<R>> {
	
	private R r;
	private String unmarshalledResource;
	private Integer size;
	
	public ResourceInfo(R r) {
		this.r = r;
	}
	
	protected String getUnmarshalledResource() {
		if(unmarshalledResource==null) {
			StringWriter stringWriter = new StringWriter();
			Resources.marshal(r, stringWriter);
			unmarshalledResource = stringWriter.toString();
		}
		return unmarshalledResource;
	}
	
	public String getResourceID() {
		return r.id();
	}
	
	public int getSize() throws UnsupportedEncodingException {
		if(size==null) {
			String unmarshalledR = getUnmarshalledResource();
			final byte[] grUTF8Bytes = unmarshalledR.getBytes("UTF-8");
			size = grUTF8Bytes.length;
		}
		return size;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    @SuppressWarnings("unchecked")
		ResourceInfo<R> other = (ResourceInfo<R>) obj;
	    return this.compareTo(other)==0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
	    int result = 1;
	    result = prime * r.id().hashCode();
	    return result;
	}

	@Override
	public int compareTo(ResourceInfo<R> other) {
		return this.r.id().compareTo(other.r.id());
	}
	
}
