/**
 * 
 */
package org.gcube.informationsystem.sweeper;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.gcube.accounting.aggregator.plugin.AccountingAggregatorPluginDeclaration;
import org.gcube.testutility.ContextTest;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.client.SmartExecutorClient;
import org.gcube.vremanagement.executor.client.SmartExecutorClientFactory;
import org.gcube.vremanagement.executor.json.SEMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ISSweeperPluginSmartExecutorSchedulerTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(ISSweeperPluginSmartExecutorSchedulerTest.class);
	
	private SmartExecutorClient smartExecutorClient;

	@Before
	public void before() throws Exception {
		setContext("/gcube");
		smartExecutorClient = SmartExecutorClientFactory.create(AccountingAggregatorPluginDeclaration.NAME);
		Assert.assertNotNull(smartExecutorClient);

	}
	
	public UUID scheduleTest(Scheduling scheduling) throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		logger.debug("Inputs : {}", inputs);
		inputs.put(ISSweeperPlugin.EXPIRING_MINUTES_TIMEOUT, 30);
		inputs.put(ISSweeperPlugin.DEAD_DAYS_TIMEOUT, 3);
		
		LaunchParameter parameter = new LaunchParameter(ISSweeperPluginDeclaration.NAME, inputs);
		if(scheduling!=null){
			parameter.setScheduling(scheduling);
		}
		
		try {
			String uuidString = smartExecutorClient.launch(SEMapper.marshal(parameter));
			return UUID.fromString(uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}
		
	}
	
	@Test
	public void cronExpPreviousMustBeTerminated() throws Exception {
		CronExpression cronExpression = new CronExpression("0 */10 * * * ?"); // every 10 minutes starting from now
		Scheduling scheduling = new Scheduling(cronExpression);
		scheduling.setGlobal(true);
		
		UUID uuid = scheduleTest(scheduling);
		logger.debug("Launched with UUID : {}", uuid);
	}
	
	//@Test
	public void unSchedule() throws Exception {
		smartExecutorClient.delete("", true);
	}
		
}
