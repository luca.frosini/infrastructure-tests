/**
 * 
 */
package org.gcube.informationsystem.sweeper;

import java.util.List;

import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.informationsystem.publisher.RegistryPublisher;
import org.gcube.informationsystem.publisher.RegistryPublisherFactory;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.icclient.ICFactory;
import org.gcube.testutility.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class RemoveGoodRITest extends ContextTest {

	private static final Logger logger = LoggerFactory.getLogger(RemoveGoodRITest.class);
	
	protected String getGCoreEndpointMinimalInfo(GCoreEndpoint gCoreEndpoint, String hostingNodeId) throws Exception{
		return String.format("%s (ID : %s - ServiceClass : %s - ServiceName : %s. Was running on %s)", 
				GCoreEndpoint.class.getSimpleName(), gCoreEndpoint.id(),
				gCoreEndpoint.profile().serviceClass(), 
				gCoreEndpoint.profile().serviceName(), hostingNodeId);
	}
	
	/* 
	 * This test is used to check if smartgears republish a RI deleted
	 * from Is for mistake, when container instance is still up and running. 
	 */
	@Test
	public void test() throws Exception {
		RegistryPublisher registryPublisher = RegistryPublisherFactory.create();
		
		String hostingNodeID = "8199b56f-460f-4d1d-a4b2-44186681972c";
		
		String condition = String.format("$resource/Profile/GHN/@UniqueID/string() eq '%s'", hostingNodeID);
		
		SimpleQuery query = ICFactory.queryFor(GCoreEndpoint.class)
				.addCondition(condition)
				.setResult("$resource");
		
		DiscoveryClient<GCoreEndpoint> client = ICFactory.clientFor(GCoreEndpoint.class);
		List<GCoreEndpoint> gCoreEndpoints = client.submit(query);
		
		for (GCoreEndpoint gCoreEndpoint : gCoreEndpoints) {
			try {
				
				logger.debug("Going to remove orphan {}", 
						getGCoreEndpointMinimalInfo(gCoreEndpoint, hostingNodeID));
				
				registryPublisher.remove(gCoreEndpoint);
				
				logger.debug("Request to remove {} successfully sent\n",
						getGCoreEndpointMinimalInfo(gCoreEndpoint, hostingNodeID));
			}catch(Exception e){
				logger.error("Unable to remove {}", 
						getGCoreEndpointMinimalInfo(gCoreEndpoint, hostingNodeID), e);
			}
		}

	}
	
}
