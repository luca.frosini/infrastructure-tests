package org.gcube.informationsystem.sweeper;

import java.util.HashMap;
import java.util.Map;

import org.gcube.testutility.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ISSweeperPluginTest extends ContextTest {

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(ISSweeperPluginTest.class);
	
	@Test
	public void testLaunch() throws Exception {
		logger.debug("Starting to test launch");
		Map<String, Object> inputs = new HashMap<String, Object>();
		ISSweeperPlugin couchDBQueryPlugin = new ISSweeperPlugin(null);
		couchDBQueryPlugin.launch(inputs);
		logger.debug("-------------- launch test finished");
	}
	
}
