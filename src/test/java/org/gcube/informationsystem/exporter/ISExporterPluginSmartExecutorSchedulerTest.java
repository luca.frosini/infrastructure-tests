/**
 * 
 */
package org.gcube.informationsystem.exporter;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.gcube.testutility.ContextTest;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.client.SmartExecutorClient;
import org.gcube.vremanagement.executor.client.SmartExecutorClientFactory;
import org.gcube.vremanagement.executor.json.SEMapper;
import org.gcube.vremanagement.executor.plugin.PluginStateEvolution;
import org.junit.Assert;
import org.junit.Test;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ISExporterPluginSmartExecutorSchedulerTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(ISExporterPluginSmartExecutorSchedulerTest.class);
	
	public UUID scheduleTest(Scheduling scheduling) throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		inputs.put(ISExporterPlugin.FILTERED_REPORT, true);
		logger.debug("Inputs : {}", inputs);

		
		LaunchParameter parameter = new LaunchParameter(ISExporterPluginDeclaration.NAME, inputs);
		if(scheduling!=null){
			parameter.setScheduling(scheduling);
		}
		
		try {
			SmartExecutorClient smartExecutorClient = SmartExecutorClientFactory.create(ISExporterPluginDeclaration.NAME);
			Assert.assertNotNull(smartExecutorClient);
			String uuidString = smartExecutorClient.launch(SEMapper.marshal(parameter));
			return UUID.fromString(uuidString);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}
		
	}
	
	//@Test
	public void production() throws Exception {
		File src = new File("src");
		File test = new File(src, "test");
		File resources = new File(test, "resources");
		
		File tokenFile = new File(resources, "production-tokens-is-exporter.json");
		
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(tokenFile);
		
		Map<String, String> contextSecondMinutes = new HashMap<>();
		contextSecondMinutes.put("/d4science.research-infrastructures.eu", "0 0");
		contextSecondMinutes.put("/d4science.research-infrastructures.eu/ParthenosVO", "30 7");
		contextSecondMinutes.put("/d4science.research-infrastructures.eu/ParthenosVO/RubRIcA", "0 15");
		contextSecondMinutes.put("/d4science.research-infrastructures.eu/ParthenosVO/PARTHENOS_Registry", "30 22");
		contextSecondMinutes.put("/d4science.research-infrastructures.eu/gCubeApps", "0 30");
		contextSecondMinutes.put("/d4science.research-infrastructures.eu/gCubeApps/Parthenos", "30 37");
		contextSecondMinutes.put("/d4science.research-infrastructures.eu/D4Research", "0 45");
		contextSecondMinutes.put("/d4science.research-infrastructures.eu/D4Research/NERLiX", "30 52");
		
		for(String context : contextSecondMinutes.keySet()){
			logger.info("\n\n\n-------------------------------------------------------------------------");
			
			String token = jsonNode.get(context).asText();
			
			ContextTest.setContext(token);
			
			CronExpression cronExpression = new CronExpression(contextSecondMinutes.get(context) + " 0/1 * * ?"); // every hour at contextSecondMinutes.get(token)
			Scheduling scheduling = new Scheduling(cronExpression, true);
			scheduling.setGlobal(true);
			
			logger.debug("{} : {} : {}", context, token, cronExpression.getCronExpression());
			
			
			
			UUID uuid = scheduleTest(scheduling);
			logger.debug("Launched with UUID : {}", uuid);
			
			logger.info("\n\n\n");
		}
		
		
	}
	
	
	@Test
	public void cronExpPreviousMustBeTerminated() throws Exception {
		
		Map<String, String> context_minutes = new HashMap<>();
		context_minutes.put("/gcube", "0");
		context_minutes.put("/gcube/devsec", "12");
		context_minutes.put("/gcube/devsec/devVRE", "24");
		context_minutes.put("/gcube/devNext", "36");
		context_minutes.put("/gcube/devNext/NextNext", "48");
		
		for(String context : context_minutes.keySet()){
			logger.info("\n\n\n-------------------------------------------------------------------------");
			ContextTest.setContextByName(context);
			
			CronExpression cronExpression = new CronExpression("0 " + context_minutes.get(context) + " 0/1 * * ?"); // every hour at tokenMinutes.get(token) minutes
			Scheduling scheduling = new Scheduling(cronExpression, true);
			scheduling.setGlobal(true);
					
			UUID uuid = scheduleTest(scheduling);
			logger.debug("Launched with UUID : {}", uuid);
			
			logger.info("\n\n\n");
		}
		
		
	}
	
	@Test
	public void getState() throws Exception {
		SmartExecutorClient smartExecutorClient = SmartExecutorClientFactory.create(ISExporterPluginDeclaration.NAME);
		Assert.assertNotNull(smartExecutorClient);
		
		PluginStateEvolution pluginStateEvolution = SEMapper.unmarshal(PluginStateEvolution.class, smartExecutorClient.getPluginStateEvolution("", 1));
		logger.debug("{}", pluginStateEvolution);
	}
	
	/*
	@Test
	public void stop() throws Exception {
		ExecutorPlugin runExecutorPlugin = new ExecutorPlugin();
		SmartExecutorPluginQuery runQuery = new SmartExecutorPluginQuery(runExecutorPlugin);
		runQuery.addConditions(ISExporterPluginDeclaration.NAME);
		
		SpecificEndpointDiscoveryFilter sedf = new SpecificEndpointDiscoveryFilter("http://pc-frosini.isti.cnr.it:8080/smart-executor/gcube/vremanagement/smart-executor");
		runQuery.setEndpointDiscoveryFilter(sedf);
		SmartExecutorProxy proxy = new ProxyBuilderImpl<SmartExecutor, SmartExecutorProxy>(runExecutorPlugin, runQuery).build();
		
		Assert.assertNotNull(proxy);
		proxy.stop("321d8640-1b9f-4529-ba23-588ff2f17625");
	}
	
	@Test
	public void launchTest() throws Exception {
		
		Map<String, Object> inputs = new HashMap<String, Object>();
		inputs.put(ISExporterPlugin.FILTERED_REPORT, true);
		logger.debug("Inputs : {}", inputs);

		LaunchParameter parameter = new LaunchParameter(ISExporterPluginDeclaration.NAME, inputs);

		
		try {
			ExecutorPlugin runExecutorPlugin = new ExecutorPlugin();
			SmartExecutorPluginQuery runQuery = new SmartExecutorPluginQuery(runExecutorPlugin);
			runQuery.addConditions(ISExporterPluginDeclaration.NAME);
			
			SpecificEndpointDiscoveryFilter sedf = new SpecificEndpointDiscoveryFilter("http://pc-frosini.isti.cnr.it:8080/smart-executor/gcube/vremanagement/smart-executor");
			runQuery.setEndpointDiscoveryFilter(sedf);
			SmartExecutorProxy proxy = new ProxyBuilderImpl<SmartExecutor, SmartExecutorProxy>(runExecutorPlugin, runQuery).build();
			
			String uuidString = proxy.launch(parameter);
			UUID uuid = UUID.fromString(uuidString);
			logger.debug("Launched with UUID : {}", uuid);
		} catch(Exception e){
			logger.error("Error launching sheduled task", e);
			throw e;
		}
		
	}
	*/

}
