package org.gcube.informationsystem.deliverable65;

import java.util.UUID;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.informationsystem.model.impl.entities.ContextImpl;
import org.gcube.informationsystem.model.impl.properties.PropagationConstraintImpl;
import org.gcube.informationsystem.model.impl.relations.IsIdentifiedByImpl;
import org.gcube.informationsystem.model.reference.entities.Context;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.AddConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.RemoveConstraint;
import org.gcube.informationsystem.resourceregistry.context.ResourceRegistryContextClient;
import org.gcube.informationsystem.resourceregistry.context.ResourceRegistryContextClientFactory;
import org.gcube.informationsystem.resourceregistry.publisher.ResourceRegistryPublisher;
import org.gcube.informationsystem.resourceregistry.publisher.ResourceRegistryPublisherFactory;
import org.gcube.informationsystem.resourceregistry.schema.ResourceRegistrySchemaClient;
import org.gcube.informationsystem.resourceregistry.schema.ResourceRegistrySchemaClientFactory;
import org.gcube.resourcemanagement.model.impl.entities.facets.CPUFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.facets.NetworkingFacetImpl;
import org.gcube.resourcemanagement.model.impl.entities.resources.HostingNodeImpl;
import org.gcube.resourcemanagement.model.impl.relations.isrelatedto.ActivatesImpl;
import org.gcube.resourcemanagement.model.reference.entities.facets.CPUFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.ContactFacet;
import org.gcube.resourcemanagement.model.reference.entities.facets.NetworkingFacet;
import org.gcube.resourcemanagement.model.reference.entities.resources.Actor;
import org.gcube.resourcemanagement.model.reference.entities.resources.EService;
import org.gcube.resourcemanagement.model.reference.entities.resources.HostingNode;
import org.gcube.resourcemanagement.model.reference.properties.AccessPolicy;
import org.gcube.resourcemanagement.model.reference.relations.consistsof.HasContact;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Activates;
import org.gcube.resourcemanagement.model.reference.relations.isrelatedto.Hosts;

public class CodeExamplesDeliverablesD65 {
	
	public void contextClientExamples() throws Exception {
		SecurityTokenProvider.instance.set("Your-Token-Here");
		ResourceRegistryContextClient resourceRegistryContextClient = ResourceRegistryContextClientFactory.create();
		
		Context parthenosInfrastructure = new ContextImpl("ParthenosInfrastructure");
		resourceRegistryContextClient.create(parthenosInfrastructure);
		
		Context parthenosVO = new ContextImpl("ParthenosVO");
		parthenosVO.setParent(parthenosInfrastructure);
		resourceRegistryContextClient.create(parthenosVO);
		
		resourceRegistryContextClient.read("30f6254c-c87a-451e-bc0f-7cfcbd94a84a");

		// resourceRegistryContextClient.exists("30f6254c-c87a-451e-bc0f-7cfcbd94a84a");
		
		
		parthenosVO.setName("ParthenosCommunity");
		Context parthenosCommunity = resourceRegistryContextClient.update(parthenosVO);
		
		parthenosCommunity.setParent((Context) null);
		resourceRegistryContextClient.update(parthenosCommunity);
		
		
		resourceRegistryContextClient.delete(parthenosCommunity);
	}
	
	public void schemaClientExamples() throws Exception {
		SecurityTokenProvider.instance.set("Your-Token-Here");
		ResourceRegistrySchemaClient resourceRegistrySchemaClient = ResourceRegistrySchemaClientFactory.create();
		
		resourceRegistrySchemaClient.create(Actor.class);

		resourceRegistrySchemaClient.create(ContactFacet.class);
		
		resourceRegistrySchemaClient.create(Hosts.class);
		
		resourceRegistrySchemaClient.create(HasContact.class);
		
		resourceRegistrySchemaClient.create(AccessPolicy.class);
		
		resourceRegistrySchemaClient.read("Actor", true);
		
	}
	
	public void instancesExamples() throws Exception {
		SecurityTokenProvider.instance.set("Your-Token-Here");
		ResourceRegistryPublisher resourceRegistryPublisher = ResourceRegistryPublisherFactory.create();
		
		CPUFacet cpuFacet = new CPUFacetImpl();
		cpuFacet.setClockSpeed("1 GHz");
		cpuFacet.setModel("Opteron");
		cpuFacet.setVendor("AMD");

		CPUFacet createdCpuFacet = resourceRegistryPublisher.create(cpuFacet);
		UUID uuid = createdCpuFacet.getHeader().getUUID(); // 69f0b376-38d2-4a85-bc63-37f9fa323f82
		
		createdCpuFacet.setClockSpeed("2 GHz");
		CPUFacet updatedCpuFacet = resourceRegistryPublisher.update(createdCpuFacet);
		
		boolean deleted = resourceRegistryPublisher.delete(createdCpuFacet);
		
	}
	
	public void instancesExamples2() throws Exception {
		SecurityTokenProvider.instance.set("Your-Token-Here");
		ResourceRegistryPublisher resourceRegistryPublisher = ResourceRegistryPublisherFactory.create();
		
		EService eService = null; 
		
		NetworkingFacet networkingFacet = new NetworkingFacetImpl();
		networkingFacet.setIPAddress("146.48.87.183");
		networkingFacet.setHostName("pc-frosini.isti.cnr.it");
		networkingFacet.setDomainName("isti.cnr.it");
		networkingFacet.setMask("255.255.248.0");
		networkingFacet.setBroadcastAddress("146.48.87.255");

		networkingFacet = resourceRegistryPublisher.createFacet(networkingFacet);

		HostingNode hostingNode = new HostingNodeImpl();

		CPUFacet cpuFacet = new CPUFacetImpl();
		cpuFacet.setClockSpeed("2 GHz");
		cpuFacet.setModel("Opteron");
		cpuFacet.setVendor("AMD");
		hostingNode.addFacet(cpuFacet);

		IsIdentifiedByImpl<Resource,Facet> isIdentifiedBy = new IsIdentifiedByImpl<Resource, Facet>(hostingNode, networkingFacet, null);
		hostingNode.addFacet(isIdentifiedBy);

		PropagationConstraint propagationConstraint = new PropagationConstraintImpl();
		propagationConstraint.setRemoveConstraint(RemoveConstraint.cascade);
		propagationConstraint.setAddConstraint(AddConstraint.unpropagate);

		Activates<HostingNode, EService> hosts = new ActivatesImpl<HostingNode, EService>(hostingNode, eService, propagationConstraint);
		hostingNode.attachResource(hosts);

		hostingNode = resourceRegistryPublisher.createResource(hostingNode);
		
		
		/* update */
		
		networkingFacet = (NetworkingFacet) hostingNode.getIdentificationFacets().get(0);
		networkingFacet.setAdditionalProperty("username", "luca.frosini");
		cpuFacet = hostingNode.getFacets(CPUFacet.class).get(0);
		cpuFacet.setClockSpeed("1 GHz");
		
		hostingNode = resourceRegistryPublisher.updateResource(hostingNode);
		
		boolean deleted = resourceRegistryPublisher.deleteResource(hostingNode);
	}
}
