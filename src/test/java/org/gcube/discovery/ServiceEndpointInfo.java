/**
 * 
 */
package org.gcube.discovery;

import org.gcube.testutility.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class ServiceEndpointInfo extends ContextTest {

	private static final Logger logger = LoggerFactory.getLogger(ServiceEndpointInfo.class);
	
	public static final String SERVICE_ENDPOINT_CATEGORY = "Database";
	public static final String SERVICE_ENDPOINT_NAME = "TwitterMonitorDatabase";
	public static final String ENTRY_NAME = "postgress";
	
	
	@Test
	public void testTwitterMonitorDatabaseDiscovery() throws Exception{
		ServiceEndpointDiscovery databaseDiscovery = new ServiceEndpointDiscovery(SERVICE_ENDPOINT_CATEGORY, SERVICE_ENDPOINT_NAME, ENTRY_NAME);
		logger.info("{}", databaseDiscovery.getUsername());
		logger.info("{}", databaseDiscovery.getPassword());
		logger.info("{}", databaseDiscovery.getURL());
	}
	
	
}
