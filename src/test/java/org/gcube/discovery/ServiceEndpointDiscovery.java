package org.gcube.discovery;

import java.security.Key;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.Property;
import org.gcube.common.resources.gcore.utils.Group;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.icclient.ICFactory;

public class ServiceEndpointDiscovery {
	
	protected String serviceEndpointCategory;
	protected String serviceEndpointName;
	protected String entryName;
	
	protected String url;
	protected String username;
	protected String password;
	
	protected Map<String, Property> propertyMap;
	
	protected void init(){
		this.propertyMap = new HashMap<String, Property>();
	}
	
	public ServiceEndpointDiscovery() throws Exception {
		this.propertyMap = new HashMap<String, Property>();
	}
	
	public ServiceEndpointDiscovery(String serviceEndpointCategory, String serviceEndpointName, String entryName) throws Exception {
		this.propertyMap = new HashMap<String, Property>();
		this.serviceEndpointCategory = serviceEndpointCategory;
		this.serviceEndpointName = serviceEndpointName;
		this.entryName = entryName;
		ServiceEndpoint serviceEndpoint = getServiceEndpoint(serviceEndpointCategory, serviceEndpointName, entryName);
		setCredentialFromServiceEndpoint(serviceEndpoint);
	}
	
	/**
	 * @return the url
	 */
	public String getURL() {
		return url;
	}

	/**
	 * @param uri the uri to set
	 */
	public void setURL(String url) {
		this.url = url;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	protected ServiceEndpoint getServiceEndpoint(String serviceEndpointCategory, String serviceEndpointName, String entryName){
		SimpleQuery query = ICFactory.queryFor(ServiceEndpoint.class);
		query.addCondition(String.format("$resource/Profile/Category/text() eq '%s'", serviceEndpointCategory));
		query.addCondition(String.format("$resource/Profile/Name/text() eq '%s'", serviceEndpointName));
		query.addCondition(String.format("$resource/Profile/AccessPoint/Interface/Endpoint/@EntryName eq '%s'", entryName));
		query.setResult("$resource");
		
		DiscoveryClient<ServiceEndpoint> client = ICFactory.clientFor(ServiceEndpoint.class);
		List<ServiceEndpoint> serviceEndpoints = client.submit(query);
		return serviceEndpoints.get(0);
	}
	
	private static String decrypt(String encrypted, Key... key) throws Exception {
		return StringEncrypter.getEncrypter().decrypt(encrypted);
	}

	protected void setCredentialFromServiceEndpoint(ServiceEndpoint serviceEndpoint) throws Exception{
		Group<AccessPoint> accessPoints = serviceEndpoint.profile().accessPoints();
		for(AccessPoint accessPoint : accessPoints){
			if(accessPoint.name().compareTo(entryName)==0){
				this.url = accessPoint.address();
				this.username = accessPoint.username();
				
				String encryptedPassword = accessPoint.password();
				String password = decrypt(encryptedPassword);
				
				this.password = password;
				this.propertyMap = accessPoint.propertyMap();
			}
		}
	}
	
}
