package org.gcube.vremanagement.executor.rest.client;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.acme.HWPluginStateNotification;
import org.acme.HelloWorldPlugin;
import org.acme.HelloWorldPluginDeclaration;
import org.gcube.testutility.ContextTest;
import org.gcube.vremanagement.executor.api.rest.SmartExecutor;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.client.SmartExecutorClientFactory;
import org.gcube.vremanagement.executor.exception.ExecutorException;
import org.gcube.vremanagement.executor.exception.PluginInstanceNotFoundException;
import org.gcube.vremanagement.executor.json.SEMapper;
import org.gcube.vremanagement.executor.plugin.PluginState;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class DefaultExecutorTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(DefaultExecutorTest.class);
	
	private SmartExecutor smartExecutor;
	
	@Before
	public void before() throws Exception {
		smartExecutor = SmartExecutorClientFactory.create(HelloWorldPluginDeclaration.NAME);
		Assert.assertNotNull(smartExecutor);
	}
	
	@Test
	public void launch() throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		LaunchParameter launchParameter = new LaunchParameter("Test", inputs);
		try {
			smartExecutor.launch(launchParameter);
		} catch (ExecutorException e) {
			// OK
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getState() throws Exception {
		UUID executionIdentifier = UUID.randomUUID();
		try {
			smartExecutor.getPluginStateEvolution(executionIdentifier, null);
		} catch (PluginInstanceNotFoundException e) {
			// OK
		} catch (Exception e) {
			throw e;
		}
		
	}
	
	@Test
	public void testOk() throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		inputs.put("Hello", "World");
		long sleepTime = TimeUnit.SECONDS.toMillis(10);
		inputs.put(HelloWorldPlugin.SLEEP_TIME, sleepTime);
		LaunchParameter launchParameter = new LaunchParameter(HelloWorldPluginDeclaration.NAME, inputs);
		
		Map<String, String> notificationInputs = new HashMap<String, String>();
		notificationInputs.put("Hello", "Hello World Notification:) :)");
		launchParameter.addPluginStateNotifications(HWPluginStateNotification.class, notificationInputs);
		try {
			UUID executionIdentifier = smartExecutor.launch(launchParameter);
			logger.debug("Execution Identifier {}", executionIdentifier.toString());
			
			Thread.sleep(TimeUnit.SECONDS.toMillis(1)); // 1 sec
			Assert.assertEquals(PluginState.RUNNING, smartExecutor.getPluginStateEvolution(executionIdentifier, null).getPluginState());
			
			
			//Thread.sleep(TimeUnit.SECONDS.toMillis(4)); // 1 + 4 sec (total : 5 sec) 
			//Assert.assertEquals(PluginState.RUNNING, smartExecutor.getPluginStateEvolution(HelloWorldPluginDeclaration.NAME, executionIdentifier, null).getPluginState());
			
			Thread.sleep(TimeUnit.SECONDS.toMillis(6)); // 5 + 6 sec (total : 11 sec)
			Assert.assertEquals(PluginState.DONE, smartExecutor.getPluginStateEvolution(executionIdentifier, null).getPluginState());
			
		} catch (Exception e) {
			logger.error("testOk Exception", e);
			throw e;
		}
		
	}
	
	@Test
	public void testSerilization() throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		inputs.put("Hello", "World");
		long sleepTime = TimeUnit.SECONDS.toMillis(20);
		inputs.put(HelloWorldPlugin.SLEEP_TIME, sleepTime);
		int minuteinterval = 2;
		// Every 5 minutes, for 12 times (one hour totally).
		Scheduling scheduling = new Scheduling(60*minuteinterval,12,true);
		scheduling.setGlobal(false);
		
		LaunchParameter launchParameter = new LaunchParameter(HelloWorldPluginDeclaration.NAME, inputs, scheduling);
		Map<String, String> notificationInputs = new HashMap<String, String>();
		notificationInputs.put("Hello", "Hello World Notification:) :)");
		launchParameter.addPluginStateNotifications(HWPluginStateNotification.class, notificationInputs);
		
		
		
		logger.debug(SEMapper.marshal(launchParameter));
	}
	
	
	@Test
	public void testScheduledTaskNotPersisted() throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		inputs.put("Hello", "World");
		long sleepTime = TimeUnit.SECONDS.toMillis(20);
		inputs.put(HelloWorldPlugin.SLEEP_TIME, sleepTime);
		int minuteinterval = 2;
		// Every 5 minutes, for 12 times (one hour totally).
		Scheduling scheduling = new Scheduling(60*minuteinterval,2,true);
		scheduling.setGlobal(false);
		
		LaunchParameter launchParameter = new LaunchParameter(HelloWorldPluginDeclaration.NAME, inputs, scheduling);
		Map<String, String> notificationInputs = new HashMap<String, String>();
		notificationInputs.put("Hello", "Hello World Notification:) :)");
		launchParameter.addPluginStateNotifications(HWPluginStateNotification.class, notificationInputs);
		
		try {
			UUID executionIdentifier = smartExecutor.launch(launchParameter);

			Thread.sleep(TimeUnit.SECONDS.toMillis(1));
			Assert.assertEquals(PluginState.RUNNING, smartExecutor.getPluginStateEvolution(executionIdentifier, null).getPluginState());
			
			Thread.sleep(TimeUnit.SECONDS.toMillis(4));
			Assert.assertEquals(PluginState.RUNNING, smartExecutor.getPluginStateEvolution(executionIdentifier, null).getPluginState());
			
			Thread.sleep(TimeUnit.SECONDS.toMillis(6));
			Assert.assertEquals(PluginState.DONE, smartExecutor.getPluginStateEvolution(executionIdentifier, null).getPluginState());
			
			// Thread.sleep(1000*60*minuteinterval); // After 5 minutes the thread should be active again
			// Assert.assertEquals(PluginState.RUNNING, smartExecutor.getPluginStateEvolution(executionIdentifier, null).getPluginState());
			// Assert.assertEquals(PluginState.RUNNING, smartExecutor.getPluginStateEvolution(executionIdentifier, 1).getPluginState());
			
			//logger.debug("Trying to stop scheduled task");
			//proxy.unSchedule(executionIdentifier, true);
			
		} catch (Exception e) {
			logger.error("", e);
			throw e;
		}
				
	}
	
	// @Test
	public void testScheduledTaskPersisted() throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		inputs.put("Hello", "World");
		long sleepTime = 20000; // 1000 millisec * 20 = 20 sec
		inputs.put(HelloWorldPlugin.SLEEP_TIME, sleepTime);
		// Every 5 minutes, for 12 times (one hour totally).
		Scheduling scheduling = new Scheduling(60*5,12,true);
		scheduling.setGlobal(true);
		
		LaunchParameter launchParameter = new LaunchParameter(HelloWorldPluginDeclaration.NAME, inputs, scheduling);
		Map<String, String> notificationInputs = new HashMap<String, String>();
		notificationInputs.put("Hello", "Hello World Notification:) :)");
		launchParameter.addPluginStateNotifications(HWPluginStateNotification.class, notificationInputs);
		try {
			UUID executionIdentifier = smartExecutor.launch(launchParameter);
			logger.debug("Task Lauched with ID : {}", executionIdentifier);
			
			Thread.sleep(1000); // 2 sec
			Assert.assertEquals(PluginState.RUNNING, smartExecutor.getPluginStateEvolution(executionIdentifier, null).getPluginState());
			
			Thread.sleep(4000); // 8 sec (total : 10 sec) 
			Assert.assertEquals(PluginState.RUNNING, smartExecutor.getPluginStateEvolution(executionIdentifier, null).getPluginState());
			
			Thread.sleep(6000); // 12 sec (total : 22 sec)
			Assert.assertEquals(PluginState.DONE, smartExecutor.getPluginStateEvolution(executionIdentifier, null).getPluginState());
			
			Thread.sleep(1000*60*5); // After 5 minutes the thread should be active again
			Assert.assertEquals(PluginState.RUNNING, smartExecutor.getPluginStateEvolution(executionIdentifier, null).getPluginState());
			Assert.assertEquals(PluginState.RUNNING, smartExecutor.getPluginStateEvolution(executionIdentifier, 1).getPluginState());
			
			smartExecutor.delete(executionIdentifier, false);
			
			
			Thread.sleep(1000*60*5); // After 5 minutes the thread should be active again. Going to unSchedule globally
			logger.debug("Trying to stop scheduled task");
			Assert.assertTrue(smartExecutor.delete(executionIdentifier, false));
			
			
			Thread.sleep(1000*60); 
			Assert.assertTrue(smartExecutor.delete(executionIdentifier, true));
			
		} catch (Exception e) {
			logger.error("testOk Exception", e);
			throw e;
		}
		
	}
	
	@Test
	public void testUnschedule() throws Exception {
		// smartExecutor.delete(UUID.fromString("c665f79a-f4b0-46d8-9aa6-51d31c2b1bea"), true);
	}

}
