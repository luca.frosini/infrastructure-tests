/**
 * 
 */
package org.gcube.vremanagement.executor.rest.client;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.acme.HelloWorldPlugin;
import org.acme.HelloWorldPluginDeclaration;
import org.gcube.common.clients.exceptions.DiscoveryException;
import org.gcube.testutility.ContextTest;
import org.gcube.vremanagement.executor.api.rest.SmartExecutor;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.client.SmartExecutorClientFactory;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class QueriedClientTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(QueriedClientTest.class);
	
	private void launchTest(SmartExecutor smartExecutor) throws Exception {
		Map<String,Object> inputs = new HashMap<String,Object>();
		inputs.put("Hello", "World");
		long sleepTime = 10000;
		inputs.put(HelloWorldPlugin.SLEEP_TIME, sleepTime);
		LaunchParameter launchParameter = new LaunchParameter(HelloWorldPluginDeclaration.NAME, inputs);
		try {
			UUID executionIdentifier = smartExecutor.launch(launchParameter);
			smartExecutor.getPluginStateEvolution(executionIdentifier, null);
		} catch(Exception e) {
			logger.error("", e);
			throw e;
		}
	}
	
	@Test
	public void testNoConditions() throws Exception {
		SmartExecutor smartExecutor = SmartExecutorClientFactory.create(HelloWorldPluginDeclaration.NAME);
		Assert.assertNotNull(smartExecutor);
		launchTest(smartExecutor);
	}
	
	@Test
	public void testWithSingleRighConditions() throws Exception {
		HelloWorldPluginDeclaration helloWorldPluginDeclaration = new HelloWorldPluginDeclaration();
		Map<String,String> capabilities = helloWorldPluginDeclaration.getSupportedCapabilities();
		
		SmartExecutor smartExecutor = SmartExecutorClientFactory.create(HelloWorldPluginDeclaration.NAME, capabilities);
		Assert.assertNotNull(smartExecutor);
		
		try {
			launchTest(smartExecutor);
		} catch(Exception e) {
			throw e;
		}
	}
	
	@Test
	public void testWithMultipleRighConditions() throws Exception {
		HelloWorldPluginDeclaration helloWorldPluginDeclaration = new HelloWorldPluginDeclaration();
		Map<String,String> capabilities = helloWorldPluginDeclaration.getSupportedCapabilities();
		capabilities.put("Version", helloWorldPluginDeclaration.getVersion());
		
		SmartExecutor smartExecutor = SmartExecutorClientFactory.create(HelloWorldPluginDeclaration.NAME, capabilities);
		Assert.assertNotNull(smartExecutor);
		
		launchTest(smartExecutor);
	}
	
	@Test
	public void testWithUnsatisfiedConditions() throws Exception {
		Map<String,String> capabilities = new HashMap<>();
		capabilities.put("Unsatisfied", "Condition");
		
		try {
			SmartExecutor smartExecutor = SmartExecutorClientFactory.create(HelloWorldPluginDeclaration.NAME,
					capabilities);
			Assert.assertNull(smartExecutor);
		} catch(DiscoveryException e) {
			// OK
		} catch(Exception e) {
			throw e;
		}
	}
	/*
	@Test
	public void testManagedPersonalfilters() throws Exception {
		HelloWorldPluginDeclaration helloWorldPluginDeclaration = new HelloWorldPluginDeclaration();
		Map<String,String> capabilities = helloWorldPluginDeclaration.getSupportedCapabilities();
		
		SmartExecutor smartExecutor = SmartExecutorClientFactory.create(HelloWorldPluginDeclaration.NAME, capabilities,
				null, new RandomGCoreEndpointQueryFilter());
		Assert.assertNotNull(smartExecutor);
		
		launchTest(smartExecutor);
	}
	
	@Test
	public void testWithSpecificSelection() throws Exception {
		HelloWorldPluginDeclaration helloWorldPluginDeclaration = new HelloWorldPluginDeclaration();
		Map<String,String> capabilities = helloWorldPluginDeclaration.getSupportedCapabilities();
		Discover discover = new Discover(Constants.SERVICE_ENTRY_NAME);
		
		List<String> endpoints = discover.getAddresses();
		
		for(String endpoint : endpoints) {
			SmartExecutor smartExecutor = SmartExecutorClientFactory.create(HelloWorldPluginDeclaration.NAME,
					capabilities, null, new SpecificGCoreEndpointQueryFilter(endpoint));
			Assert.assertNotNull(smartExecutor);
			
			launchTest(smartExecutor);
		}
		
	}
	*/
}
