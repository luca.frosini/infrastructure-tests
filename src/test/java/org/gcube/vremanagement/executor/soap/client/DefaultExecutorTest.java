/**
 * 
 */
package org.gcube.vremanagement.executor.soap.client;

import org.gcube.testutility.ContextTest;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class DefaultExecutorTest extends ContextTest {
	
	
//	private static Logger logger = LoggerFactory.getLogger(DefaultExecutorTest.class);
//	
//	private SmartExecutorProxy proxy;
//	
//	@Before
//	public void before() throws Exception{
//		proxy = ExecutorPlugin.getExecutorProxy(HelloWorldPluginDeclaration.NAME).build();
//		Assert.assertNotNull(proxy);
//	}
//	
//	@Test
//	public void launch() {
//		Map<String, Object> inputs = new HashMap<String, Object>();
//		LaunchParameter launchParameter = new LaunchParameter("Test", inputs);
//		try {
//			proxy.launch(launchParameter);
//		} catch (Exception e) {
//			Assert.assertEquals(PluginNotFoundException.class, e.getCause().getClass());
//		}
//	}
//
//	@Test
//	public void getState() throws Exception {
//		String executionIdentifier = UUID.randomUUID().toString();
//		try {
//			proxy.getStateEvolution(executionIdentifier);
//		} catch (Exception e) {
//			Assert.assertEquals(PluginInstanceNotFoundException.class, e.getCause().getClass());
//		}
//	}
//	
//	@Test
//	public void testOk() throws Exception {
//		Map<String, Object> inputs = new HashMap<String, Object>();
//		inputs.put("Hello", "World");
//		long sleepTime = TimeUnit.SECONDS.toMillis(10);
//		inputs.put(HelloWorldPlugin.SLEEP_TIME, sleepTime);
//		LaunchParameter launchParameter = new LaunchParameter(HelloWorldPluginDeclaration.NAME, inputs);
//		
//		Map<String, String> notificationInputs = new HashMap<String, String>();
//		notificationInputs.put("Hello", "Hello World Notification:) :)");
//		launchParameter.addPluginStateNotifications(HWPluginStateNotification.class, notificationInputs);
//		try {
//			String executionIdentifier = proxy.launch(launchParameter);
//			logger.debug("Execution Identifier {}", executionIdentifier);
//			
//			Thread.sleep(TimeUnit.SECONDS.toMillis(1)); // 1 sec
//			Assert.assertEquals(PluginState.RUNNING, proxy.getStateEvolution(executionIdentifier).getPluginState());
//			
//			
//			Thread.sleep(TimeUnit.SECONDS.toMillis(4)); // 1 + 4 sec (total : 5 sec) 
//			Assert.assertEquals(PluginState.RUNNING, proxy.getStateEvolution(executionIdentifier).getPluginState());
//			
//			Thread.sleep(TimeUnit.SECONDS.toMillis(6)); // 5 + 6 sec (total : 11 sec)
//			Assert.assertEquals(PluginState.DONE, proxy.getStateEvolution(executionIdentifier).getPluginState());
//			
//		} catch (Exception e) {
//			logger.error("testOk Exception", e);
//			throw e;
//		}
//		
//	}
//	
//	@Test
//	public void testSerilization() throws Exception {
//		Map<String, Object> inputs = new HashMap<String, Object>();
//		inputs.put("Hello", "World");
//		long sleepTime = TimeUnit.SECONDS.toMillis(20);
//		inputs.put(HelloWorldPlugin.SLEEP_TIME, sleepTime);
//		int minuteinterval = 2;
//		// Every 5 minutes, for 12 times (one hour totally).
//		Scheduling scheduling = new Scheduling(60*minuteinterval,12,true);
//		scheduling.setGlobal(false);
//		
//		LaunchParameter launchParameter = new LaunchParameter(HelloWorldPluginDeclaration.NAME, inputs, scheduling);
//		Map<String, String> notificationInputs = new HashMap<String, String>();
//		notificationInputs.put("Hello", "Hello World Notification:) :)");
//		launchParameter.addPluginStateNotifications(HWPluginStateNotification.class, notificationInputs);
//		
//		
//		
//		logger.debug(SEMapper.marshal(launchParameter));
//	}
//	
//	
//	@Test
//	public void testScheduledTaskNotPersisted() throws Exception {		
//		Map<String, Object> inputs = new HashMap<String, Object>();
//		inputs.put("Hello", "World");
//		long sleepTime = TimeUnit.SECONDS.toMillis(10);
//		inputs.put(HelloWorldPlugin.SLEEP_TIME, sleepTime);
//		int minuteinterval = 2;
//		// Every 5 minutes, for 12 times (one hour totally).
//		Scheduling scheduling = new Scheduling(60*minuteinterval,12,true);
//		scheduling.setGlobal(false);
//		
//		LaunchParameter launchParameter = new LaunchParameter(HelloWorldPluginDeclaration.NAME, inputs, scheduling);
//		Map<String, String> notificationInputs = new HashMap<String, String>();
//		notificationInputs.put("Hello", "Hello World Notification:) :)");
//		launchParameter.addPluginStateNotifications(HWPluginStateNotification.class, notificationInputs);
//		try {
//			String executionIdentifier = proxy.launch(launchParameter);
//
//			Thread.sleep(TimeUnit.SECONDS.toMillis(1));
//			Assert.assertEquals(PluginState.RUNNING, proxy.getStateEvolution(executionIdentifier).getPluginState());
//			
//			Thread.sleep(TimeUnit.SECONDS.toMillis(4)); 
//			Assert.assertEquals(PluginState.RUNNING, proxy.getStateEvolution(executionIdentifier).getPluginState());
//			
//			Thread.sleep(TimeUnit.SECONDS.toMillis(6)); 
//			Assert.assertEquals(PluginState.DONE, proxy.getStateEvolution(executionIdentifier).getPluginState());
//			
//			// Thread.sleep(1000*60*minuteinterval); // After 5 minutes the thread should be active again
//			// Assert.assertEquals(PluginState.RUNNING, proxy.getStateEvolution(executionIdentifier).getPluginState());
//			// Assert.assertEquals(PluginState.RUNNING, proxy.getIterationStateEvolution(executionIdentifier, 1).getPluginState());
//			
//			//logger.debug("Trying to stop scheduled task");
//			//proxy.unSchedule(executionIdentifier, true);
//			
//		} catch (Exception e) {
//			logger.error("testOk Exception", e);
//			throw e;
//		}
//				
//	}
//	
//	//@Test
//	public void testScheduledTaskPersisted() throws Exception {
//		proxy = ExecutorPlugin.getExecutorProxy((new HelloWorldPluginDeclaration()).getName()).build();
//		Assert.assertNotNull(proxy);
//		
//		Map<String, Object> inputs = new HashMap<String, Object>();
//		inputs.put("Hello", "World");
//		long sleepTime = TimeUnit.SECONDS.toMillis(20); // 1000 millisec * 20 = 20 sec
//		inputs.put(HelloWorldPlugin.SLEEP_TIME, sleepTime);
//		// Every 5 minutes, for 12 times (one hour totally).
//		Scheduling scheduling = new Scheduling(60*5,2,true);
//		scheduling.setGlobal(true);
//		
//		LaunchParameter launchParameter = new LaunchParameter(HelloWorldPluginDeclaration.NAME, inputs, scheduling);
//		Map<String, String> notificationInputs = new HashMap<String, String>();
//		notificationInputs.put("Hello", "Hello World Notification:) :)");
//		launchParameter.addPluginStateNotifications(HWPluginStateNotification.class, notificationInputs);
//		try {
//			String executionIdentifier = proxy.launch(launchParameter);
//			logger.debug("Task Lauched with ID : {}", executionIdentifier);
//			
//			Thread.sleep(TimeUnit.SECONDS.toMillis(1));
//			Assert.assertEquals(PluginState.RUNNING, proxy.getStateEvolution(executionIdentifier).getPluginState());
//			
//			Thread.sleep(TimeUnit.SECONDS.toMillis(4)); 
//			Assert.assertEquals(PluginState.RUNNING, proxy.getStateEvolution(executionIdentifier).getPluginState());
//			
//			Thread.sleep(TimeUnit.SECONDS.toMillis(6)); 
//			Assert.assertEquals(PluginState.DONE, proxy.getStateEvolution(executionIdentifier).getPluginState());
//			
//			/*
//			Thread.sleep(1000*60*5); // After 5 minutes the thread should be active again
//			Assert.assertEquals(PluginState.RUNNING, proxy.getStateEvolution(executionIdentifier).getPluginState());
//			Assert.assertEquals(PluginState.RUNNING, proxy.getIterationStateEvolution(executionIdentifier, 1).getPluginState());
//			
//			proxy.stop(executionIdentifier);
//			
//			
//			Thread.sleep(1000*60*5); // After 5 minutes the thread should be active again. Goign to unschedule globally
//			logger.debug("Trying to stop scheduled task");
//			Assert.assertTrue(proxy.unSchedule(executionIdentifier, false));
//			*/
//			
//			Thread.sleep(1000*60); 
//			Assert.assertTrue(proxy.unSchedule(executionIdentifier, true));
//			
//		} catch (Exception e) {
//			logger.error("testOk Exception", e);
//			throw e;
//		}
//		
//	}
//	
//	@Test
//	public void testUnschedule() throws Exception {
//		//proxy.unSchedule("542ddb03-d8d7-4913-8700-2acfa74c7485", true);
//		//proxy.unSchedule("37d8f020-cdeb-4b2c-b245-4deabc1fe149", false);
//	}

}
