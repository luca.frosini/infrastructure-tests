/**
 * 
 */
package org.gcube.testutility;

import java.io.StringWriter;

import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.Resources;
import org.gcube.informationsystem.publisher.RegistryPublisher;
import org.gcube.informationsystem.publisher.RegistryPublisherFactory;
import org.gcube.informationsystem.publisher.exception.RegistryNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class TestUtility {

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(TestUtility.class);
	
	
	/**
	 * Publish the provided resource on current scope
	 * @param resource to be published
	 * @throws RegistryNotFoundException if the Registry is not found so the
	 * resource has not be published
	 */
	public static void publishResource(Resource resource) throws Exception {
		StringWriter stringWriter = new StringWriter();
		Resources.marshal(resource, stringWriter);
		
		RegistryPublisher registryPublisher = RegistryPublisherFactory.create();
		
	    try {
	    	logger.debug("Trying to publish to {}:\n{}", ContextTest.getCurrentContext(), stringWriter);
	    	registryPublisher.create(resource);
		} catch (Exception e) {
			logger.error("The resource was not published", e);
			throw e;
		}
	}
	
	/**
	 * Remove the resource from IS from curretn scope
	 * @param resource to be unpublished
	 * @throws RegistryNotFoundException if the Registry is not found so the
	 * resource has not be published
	 */
	public static void unPublishResource(Resource resource) throws Exception {
		//StringWriter stringWriter = new StringWriter();
		//Resources.marshal(resource, stringWriter);
		
		RegistryPublisher registryPublisher = RegistryPublisherFactory.create();
	    
		String id = resource.id();
	    logger.debug("Trying to remove {} with ID {} from {}", resource.getClass().getSimpleName(), id, ContextTest.getCurrentContext());
	    
		registryPublisher.remove(resource);
		
		logger.debug("{} with ID {} removed successfully", resource.getClass().getSimpleName(), id);
	}
	
}
