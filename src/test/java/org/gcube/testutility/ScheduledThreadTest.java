package org.gcube.testutility;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.gcube.documentstore.persistence.ExecutorUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScheduledThreadTest {
	
	public static final Logger logger = LoggerFactory.getLogger(ScheduledThreadTest.class);
	
	public static final long TIME_RELOAD_CONFIGURATION = 2; 
	
	public ScheduledThreadTest() {
		
	}
	
	protected ScheduledFuture<?> futureReload;
	
	
	protected void reloadConfiguration() {
		futureReload = ExecutorUtils.CONFIGURATION_REDISCOVERY_POOL.scheduleAtFixedRate(new ReloaderThread(),
				3, TIME_RELOAD_CONFIGURATION, TimeUnit.SECONDS);
	}
	
	public class ReloaderThread extends Thread {

		public ReloaderThread() {
			super();
		}

		public void run() {
			logger.debug("Hello, I'm going to fail");
			throw new RuntimeException();
		}

	}
	
	@Test
	public void test() throws InterruptedException {
		reloadConfiguration();
		Thread.sleep(TimeUnit.MINUTES.toMillis(3));
		
	}
	
}
