package org.gcube.hl;

import java.util.List;

import org.gcube.common.homelibrary.home.Home;
import org.gcube.common.homelibrary.home.HomeLibrary;
import org.gcube.common.homelibrary.home.HomeManager;
import org.gcube.common.homelibrary.home.HomeManagerFactory;
import org.gcube.common.homelibrary.home.User;
import org.gcube.common.homelibrary.home.workspace.Workspace;
import org.gcube.common.homelibrary.home.workspace.WorkspaceFolder;
import org.gcube.common.homelibrary.home.workspace.WorkspaceItem;
import org.gcube.common.homelibrary.home.workspace.WorkspaceSharedFolder;
import org.gcube.common.homelibrary.home.workspace.catalogue.WorkspaceCatalogue;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HLCatalogueSpecialFolderTest {
	
	private static final Logger logger = LoggerFactory.getLogger(HLCatalogueSpecialFolderTest.class);
	
	@Test
	public void getHLCatalogueFolderForVRE() throws Exception {
		ContextTest.setContext("2d23f22a-a391-41e3-afee-3825953e1ad4-843339462");
		String username = ContextUtility.getUsername();
		HomeManagerFactory factory = HomeLibrary.getHomeManagerFactory();
		HomeManager manager = factory.getHomeManager();
		User user = manager.getUser(username);
		@SuppressWarnings("deprecation")
		Home home = manager.getHome(user);
		Workspace ws = home.getWorkspace();

		
		WorkspaceSharedFolder vreFolder = ws.getVREFolderByScope(ContextUtility.getCurrentContext());
		WorkspaceCatalogue workspaceCatalogue = vreFolder.getVRECatalogue();
		
		logger.debug("{} {} {}", workspaceCatalogue.getId(), workspaceCatalogue.getPath(), workspaceCatalogue.isHidden()? " (hidden)":"");
		
		List<WorkspaceItem> workspaceItems = workspaceCatalogue.getChildren(true);
		for(WorkspaceItem workspaceItem : workspaceItems) {
			logger.debug("{} {} {}", workspaceItem.getId(), workspaceItem.getPath(), workspaceItem.isHidden()? " (hidden)":"");
			if(workspaceItem.getId().compareTo("d81bd0dc-40c4-4a4f-b4c2-6e0642c5215d")==0) {
				WorkspaceFolder workspaceFolder = (WorkspaceFolder) workspaceItem;
				List<WorkspaceItem> stockWorkspaceItems = workspaceFolder.getChildren(true);
				for(WorkspaceItem stockWorkspaceItem : stockWorkspaceItems) {
					logger.debug("{} {} {}", stockWorkspaceItem.getId(), stockWorkspaceItem.getPath(), stockWorkspaceItem.isHidden()? " (hidden)":"");
					// stockWorkspaceItem.setHidden(false);
					//stockWorkspaceItem.remove();
				}
				
				//workspaceItem.remove();
			}
		}

	}
		
}
