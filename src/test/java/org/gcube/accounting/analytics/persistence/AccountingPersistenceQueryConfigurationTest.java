/**
 * 
 */
package org.gcube.accounting.analytics.persistence;

import org.gcube.accounting.analytics.persistence.couchbase.AccountingPersistenceQueryCouchBase;
import org.gcube.accounting.persistence.AccountingPersistenceConfiguration;
import org.gcube.testutility.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class AccountingPersistenceQueryConfigurationTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(AccountingPersistenceQueryConfigurationTest.class);

	@Test
	public void accountingPersistenceQueryCouchDBConfigurationTest() throws Exception {
		AccountingPersistenceBackendQueryConfiguration acbqc = new AccountingPersistenceBackendQueryConfiguration(
				AccountingPersistenceQueryCouchBase.class);
		logger.debug("{}", acbqc);
	}

	// @Test
	public void getUsernamePasswordForScopes() throws Exception {
		try {
			AccountingPersistenceBackendQueryConfiguration persitenceConfiguration = new AccountingPersistenceBackendQueryConfiguration(
					AccountingPersistenceQueryCouchBase.class);
			String uri = persitenceConfiguration.getProperty(AccountingPersistenceConfiguration.URL_PROPERTY_KEY);
			String username = persitenceConfiguration
					.getProperty(AccountingPersistenceConfiguration.USERNAME_PROPERTY_KEY);
			String password = persitenceConfiguration
					.getProperty(AccountingPersistenceConfiguration.PASSWORD_PROPERTY_KEY);
			logger.debug("{} {} - {} : {}", ContextTest.getCurrentContext(), uri, username, password);
		} catch (IndexOutOfBoundsException e) {
			logger.debug("No AccountingPersistenceConfiguration : \n {} {} \n\n", e.getClass().getName(),
					e.getMessage());
		} catch (Exception e) {
			logger.error("Error getting AccountingPersistenceConfiguration", e);
			throw e;
		}
	}

}
