/**
 * 
 */
package org.gcube.accounting.analytics.persistence.couchbase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import org.gcube.accounting.analytics.Filter;
import org.gcube.accounting.analytics.Info;
import org.gcube.accounting.analytics.NumberedFilter;
import org.gcube.accounting.analytics.TemporalConstraint;
import org.gcube.accounting.analytics.TemporalConstraint.AggregationMode;
import org.gcube.accounting.analytics.persistence.AccountingPersistenceBackendQueryConfiguration;
import org.gcube.accounting.datamodel.aggregation.AggregatedStorageUsageRecord;
import org.gcube.testutility.ContextTest;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class AccountingPersistenceQueryCouchBaseTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(AccountingPersistenceQueryCouchBaseTest.class);
	
	protected AccountingPersistenceQueryCouchBase accountingPersistenceQueryCouchBase;
	
	public class ExtendedInfo extends Info {
		
		protected String key;
		
		/**
		 * @return the key
		 */
		public String getKey() {
			return key;
		}

		/**
		 * @param key the key to set
		 */
		public void setKey(String key) {
			this.key = key;
		}

		public ExtendedInfo(String key, Calendar calendar, JSONObject value){
			super(calendar, value);
			this.key = key;
		}
		
		public String toString(){
			String info = String .format("Key : %s, %s ", key, super.toString());
			return info;
		}
		
	}
	
	
	@Before
	public void before() throws Exception{
		AccountingPersistenceBackendQueryConfiguration configuration = new 
				AccountingPersistenceBackendQueryConfiguration(AccountingPersistenceQueryCouchBase.class);
		accountingPersistenceQueryCouchBase = new AccountingPersistenceQueryCouchBase();
		accountingPersistenceQueryCouchBase.prepareConnection(configuration);
	}
	
	
	public void printMap(Map<Calendar, Info> map){
		for(Info info : map.values()){
			logger.debug("{}", info);
		}
	}
	
	@Test
	public void test() throws Exception {
		Calendar startTime = Calendar.getInstance();
		startTime.set(2016, Calendar.JANUARY, 1);
		Calendar endTime = Calendar.getInstance();
		List<Filter> filters = new ArrayList<Filter>();
		
		Filter filter = 
		new Filter(AggregatedStorageUsageRecord.CONSUMER_ID, "gianpaolo.coro");
		filters.add(filter);
		
		TemporalConstraint temporalConstraint = 
				new TemporalConstraint(startTime.getTimeInMillis(), 
						endTime.getTimeInMillis(), AggregationMode.DAILY);
		
		Class<AggregatedStorageUsageRecord> clz = 
				AggregatedStorageUsageRecord.class;
		
		SortedSet<NumberedFilter>  set = 
				accountingPersistenceQueryCouchBase.getNextPossibleValues(
				clz, temporalConstraint, filters, 
				AggregatedStorageUsageRecord.OPERATION_TYPE, null);
		
		logger.debug("{}", set);

	}
}
