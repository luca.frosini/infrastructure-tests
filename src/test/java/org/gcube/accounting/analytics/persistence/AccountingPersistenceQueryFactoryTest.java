/**
 * 
 */
package org.gcube.accounting.analytics.persistence;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.gcube.accounting.analytics.Filter;
import org.gcube.accounting.analytics.Info;
import org.gcube.accounting.analytics.TemporalConstraint;
import org.gcube.accounting.analytics.TemporalConstraint.AggregationMode;
import org.gcube.accounting.datamodel.UsageRecord;
import org.gcube.accounting.datamodel.aggregation.AggregatedServiceUsageRecord;
import org.gcube.accounting.datamodel.aggregation.AggregatedStorageUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.ServiceUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.StorageUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.TestUsageRecord;
import org.gcube.documentstore.records.AggregatedRecord;
import org.gcube.testutility.ContextTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 */
public class AccountingPersistenceQueryFactoryTest extends ContextTest {

	private static Logger logger = LoggerFactory
			.getLogger(AccountingPersistenceQueryFactoryTest.class);

	protected AccountingPersistenceQuery apq;

	@Before
	public void before() throws Exception {
		apq = AccountingPersistenceQueryFactory.getInstance();
	}

	@Test
	public void testNullFilters() throws Exception {
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(startTime.getTimeInMillis()
				- (1000 * 60 * 60 * 24 * 3));
		Calendar endTime = Calendar.getInstance();
		TemporalConstraint temporalConstraint = new TemporalConstraint(
				startTime.getTimeInMillis(), endTime.getTimeInMillis(),
				AggregationMode.SECONDLY);
		List<Filter> filters = null;
		Class<? extends AggregatedRecord<?, ?>> clz = AggregatedServiceUsageRecord.class;
		Map<Calendar, Info> infos = apq.getTimeSeries(clz, temporalConstraint,
				filters, true);
		Assert.assertTrue(infos != null);
		for (Info info : infos.values()) {
			logger.debug(info.toString());
		}
	}

	@Test
	public void testEmptyFilters() throws Exception {
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(startTime.getTimeInMillis()
				- (1000 * 60 * 60 * 24 * 3));
		Calendar endTime = Calendar.getInstance();
		TemporalConstraint temporalConstraint = new TemporalConstraint(
				startTime.getTimeInMillis(), endTime.getTimeInMillis(),
				AggregationMode.SECONDLY);
		List<Filter> filters = new ArrayList<Filter>();
		Class<? extends AggregatedRecord<?, ?>> clz = AggregatedServiceUsageRecord.class;
		Map<Calendar, Info> infos = apq.getTimeSeries(clz, temporalConstraint,
				filters, true);
		Assert.assertTrue(infos != null);
		for (Info info : infos.values()) {
			logger.debug(info.toString());
		}
	}

	@Test
	public void testFakeFilters() throws Exception {
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(startTime.getTimeInMillis()
				- (1000 * 60 * 60 * 24 * 3));
		Calendar endTime = Calendar.getInstance();
		TemporalConstraint temporalConstraint = new TemporalConstraint(
				startTime.getTimeInMillis(), endTime.getTimeInMillis(),
				AggregationMode.SECONDLY);
		List<Filter> filters = new ArrayList<Filter>();
		filters.add(new Filter("AUX", "AUX"));
		Class<? extends AggregatedRecord<?, ?>> clz = AggregatedServiceUsageRecord.class;
		apq.getTimeSeries(clz, temporalConstraint, filters, true);
	}

	@Test
	public void testFiltersGoodKeyFakeValue() throws Exception {
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(startTime.getTimeInMillis()
				- (1000 * 60 * 60 * 24 * 3));
		Calendar endTime = Calendar.getInstance();
		TemporalConstraint temporalConstraint = new TemporalConstraint(
				startTime.getTimeInMillis(), endTime.getTimeInMillis(),
				AggregationMode.SECONDLY);
		List<Filter> filters = new ArrayList<Filter>();
		filters.add(new Filter(ServiceUsageRecord.SERVICE_CLASS, "AUX"));
		Class<? extends AggregatedRecord<?, ?>> clz = AggregatedServiceUsageRecord.class;
		Map<Calendar, Info> infos = apq.getTimeSeries(clz, temporalConstraint,
				filters, true);
		Assert.assertTrue(infos != null);
		Assert.assertTrue(infos.isEmpty());
	}

	public static final long timeout = 5000;
	public static final TimeUnit timeUnit = TimeUnit.MILLISECONDS;

	@Test
	public void testFiltersGoodKeyGoodValue() throws Exception {
		Calendar startTime = Calendar.getInstance();
		startTime.setTimeInMillis(startTime.getTimeInMillis()
				- (1000 * 60 * 60 * 24 * 365));
		Calendar endTime = Calendar.getInstance();
		TemporalConstraint temporalConstraint = new TemporalConstraint(
				startTime.getTimeInMillis(), endTime.getTimeInMillis(),
				AggregationMode.SECONDLY);
		List<Filter> filters = new ArrayList<Filter>();
		filters.add(new Filter(ServiceUsageRecord.SERVICE_CLASS,
				TestUsageRecord.TEST_SERVICE_CLASS));
		Class<? extends AggregatedRecord<?, ?>> clz = AggregatedServiceUsageRecord.class;
		Map<Calendar, Info> infos = apq.getTimeSeries(clz, temporalConstraint,
				filters, true);
		Assert.assertTrue(infos != null);
		Assert.assertTrue(!infos.isEmpty());
		for (Info info : infos.values()) {
			logger.debug(info.toString());
		}

		filters = new ArrayList<Filter>();
		filters.add(new Filter(StorageUsageRecord.RESOURCE_OWNER,
				TestUsageRecord.TEST_RESOUCE_OWNER));
		clz = AggregatedStorageUsageRecord.class;
		infos = apq.getTimeSeries(clz, temporalConstraint, filters, true);
		Assert.assertTrue(infos != null);
		Assert.assertTrue(!infos.isEmpty());
		for (Info info : infos.values()) {
			logger.debug(info.toString());
		}

	}

	// @ Test
	public void testRange() throws Exception {
		Calendar startTime = Calendar.getInstance();
		startTime.set(Calendar.MONTH, Calendar.AUGUST);
		startTime.set(Calendar.DAY_OF_MONTH, 7);

		Calendar endTime = Calendar.getInstance();
		endTime.set(Calendar.MONTH, Calendar.SEPTEMBER);
		endTime.set(Calendar.DAY_OF_MONTH, 7);
		TemporalConstraint temporalConstraint = new TemporalConstraint(
				startTime.getTimeInMillis(), endTime.getTimeInMillis(),
				AggregationMode.DAILY);
		logger.trace("{} : {}", TemporalConstraint.class.getSimpleName(),
				temporalConstraint.toString());

		List<Filter> filters = new ArrayList<Filter>();
		/*
		 * filters.add(new Filter(ServiceUsageRecord.SERVICE_CLASS,
		 * TestUsageRecord.TEST_SERVICE_CLASS)); Map<Calendar, Info> infos =
		 * apq.query(AggregatedServiceUsageRecord.class, temporalConstraint,
		 * filters); Assert.assertTrue(infos!=null);
		 * Assert.assertTrue(!infos.isEmpty()); for(Info info : infos.values()){
		 * logger.debug(info.toString()); }
		 */

		filters = new ArrayList<Filter>();
		filters.add(new Filter(UsageRecord.CONSUMER_ID, "gianpaolo.coro"));

		Class<? extends AggregatedRecord<?, ?>> clz = AggregatedStorageUsageRecord.class;
		Map<Calendar, Info> infos = apq.getTimeSeries(clz, temporalConstraint,
				filters, true);
		Assert.assertTrue(infos != null);
		Assert.assertTrue(!infos.isEmpty());

		for (Info info : infos.values()) {
			logger.debug(info.toString());
		}

	}

	@Test
	public void getKeysTest() throws Exception {
		Set<String> keys = AccountingPersistenceQuery
				.getQuerableKeys(AggregatedServiceUsageRecord.class);
		logger.debug("Got keys : {}", keys);

		Set<String> required = AggregatedServiceUsageRecord.class.newInstance()
				.getRequiredFields();
		logger.debug("Required fields : {}", required);

		Assert.assertTrue(required.containsAll(keys));

	}

	@Test
	public void testMontly() throws Exception {
		Calendar startTime = Calendar.getInstance();
		startTime.set(Calendar.MONTH, Calendar.JANUARY);
		startTime.set(Calendar.DAY_OF_MONTH, 1);

		Calendar endTime = Calendar.getInstance();
		endTime.set(Calendar.MONTH, Calendar.DECEMBER);
		endTime.set(Calendar.DAY_OF_MONTH, 31);

		TemporalConstraint temporalConstraint = new TemporalConstraint(
				startTime.getTimeInMillis(), endTime.getTimeInMillis(),
				AggregationMode.MONTHLY);
		logger.trace("{} : {}", TemporalConstraint.class.getSimpleName(),
				temporalConstraint.toString());

		List<Filter> filters = new ArrayList<Filter>();
		AccountingPersistenceBackendQuery apq = AccountingPersistenceBackendQueryFactory
				.getInstance();
		Map<Calendar, Info> infos = apq.getTimeSeries(
				AggregatedStorageUsageRecord.class, temporalConstraint,
				filters);

		Assert.assertTrue(infos != null);
		Assert.assertTrue(!infos.isEmpty());

		for (Info info : infos.values()) {
			logger.debug(info.toString());
		}

	}

}
