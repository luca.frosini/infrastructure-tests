/**
 * 
 */
package org.gcube.accounting.datamodel.usagerecords;

import org.gcube.accounting.persistence.AccountingPersistence;
import org.gcube.accounting.persistence.AccountingPersistenceFactory;
import org.gcube.documentstore.exception.InvalidValueException;
import org.gcube.documentstore.exception.NotAggregatableRecordsExceptions;
import org.gcube.documentstore.persistence.PersistenceBackendFactory;
import org.gcube.testutility.ContextTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class AccountingTest extends ContextTest{
	
	private static Logger logger = LoggerFactory.getLogger(AccountingTest.class);
	
	private AccountingPersistence accountingPersistence;
	
	@Before
	public void before() throws Exception{
		PersistenceBackendFactory.forceImmediateRediscovery(ContextTest.getCurrentContext());
		accountingPersistence = AccountingPersistenceFactory.getPersistence();
	}
	
	@After
	public void after(){
		try {
			accountingPersistence.flush();
		} catch (Exception e) {
			logger.error("Error flushing Buffered Records", e);
		}
	}
	
	@Test
	public void accountingServiceUsageRecordStressTest() throws InvalidValueException, NotAggregatableRecordsExceptions {
		
		for(int i=0; i<1000; i++){
			ServiceUsageRecord sur = TestUsageRecord.createTestServiceUsageRecord();
			sur.setScope(TestUsageRecord.TEST_SCOPE);
			accountingPersistence.account(sur);
		}

	}
	
	
	@Test
	public void accountingStorageUsageRecordStressTest() throws InvalidValueException, NotAggregatableRecordsExceptions {
		for(int i=0; i<1000; i++){
			StorageUsageRecord sur = org.gcube.accounting.datamodel.usagerecords.TestUsageRecord.createTestStorageUsageRecord();
			sur.setScope(TestUsageRecord.TEST_SCOPE);
			accountingPersistence.account(sur);
		}
	}
	
	@Test
	public void accountingJobUsageRecordStressTest() throws InvalidValueException, NotAggregatableRecordsExceptions {
		for(int i=0; i<1000; i++){
			JobUsageRecord jur = TestUsageRecord.createTestJobUsageRecord();
			jur.setScope(TestUsageRecord.TEST_SCOPE);
			accountingPersistence.account(jur);
		}
	}
	
	@Test
	public void accountingPortletUsageRecordStressTest() throws InvalidValueException, NotAggregatableRecordsExceptions {
		for(int i=0; i<1000; i++){
			PortletUsageRecord pur = TestUsageRecord.createTestPortletUsageRecord();
			pur.setScope(TestUsageRecord.TEST_SCOPE);
			accountingPersistence.account(pur);
		}
	}
	
}
