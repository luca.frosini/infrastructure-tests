/**
 * 
 *//*
package org.gcube.accounting.persistence;

import java.io.File;
import java.io.StringWriter;
import java.util.Calendar;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.Resources;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.documentstore.persistence.PersistenceBackend;
import org.gcube.documentstore.persistence.PersistenceBackendFactory;
import org.gcube.documentstore.persistence.PersistenceCouchDB;
import org.gcube.informationsystem.publisher.RegistryPublisher;
import org.gcube.informationsystem.publisher.RegistryPublisherFactory;
import org.gcube.testutility.ScopedTest;
import org.gcube.testutility.TestUtility;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

*//**
 * @author Luca Frosini (ISTI - CNR)
 *
 *//*
public class PersistenceCouchDBTest extends ScopedTest {
	
	private static final Logger logger = LoggerFactory.getLogger(PersistenceCouchDBTest.class);
	
	public static PersistenceBackend getPersistence(){
		PersistenceBackendFactory.setFallbackLocation(null);
		return PersistenceBackendFactory.getPersistenceBackend(TestUtility.getScope());
	}
	
	@Test
	public void persistenceIsCouchDB() {
		PersistenceBackend persistence = getPersistence();
		Assert.assertTrue(persistence instanceof PersistenceCouchDB);
	}
	
	private static void publishResource(Resource resource) throws Exception {
		StringWriter stringWriter = new StringWriter();
		Resources.marshal(resource, stringWriter);
		
		RegistryPublisher registryPublisher = RegistryPublisherFactory.create();
		
	    try {
	    	logger.debug("Trying to publish to {}:\n{}", TestUtility.getScope(), stringWriter);
	    	registryPublisher.create(resource);
		} catch (Exception e) {
			logger.error("The resource was not published", e);
			throw e;
		}
	}
	
	private static void unPublishResource(Resource resource) throws Exception {
		//StringWriter stringWriter = new StringWriter();
		//Resources.marshal(resource, stringWriter);
		
		RegistryPublisher registryPublisher = RegistryPublisherFactory.create();
	    
		String id = resource.id();
	    logger.debug("Trying to remove {} with ID {} from {}", 
	    		resource.getClass().getSimpleName(), id, 
	    		TestUtility.getScope());
	    
		registryPublisher.remove(resource);
		
		logger.debug("{} with ID {} removed successfully", resource.getClass().getSimpleName(), id);
	}
	
	public void testScopeRecheck() throws Exception {
		
		ServiceEndpoint serviceEndpoint = null;
		try {
			AccountingPersistenceConfiguration persitenceConfiguration = 
					new AccountingPersistenceConfiguration(PersistenceCouchDB.class);
			serviceEndpoint = persitenceConfiguration.getServiceEndpoint(
					AccountingPersistenceConfiguration.SERVICE_ENDPOINT_CATEGORY, 
					AccountingPersistenceConfiguration.SERVICE_ENDPOINT_NAME, 
					PersistenceCouchDB.class);
			unPublishResource(serviceEndpoint);
		} catch(IndexOutOfBoundsException e){
			SecurityTokenProvider.instance.set(TestUtility.PARENT_TOKEN);
			AccountingPersistenceConfiguration persitenceConfiguration = 
					new AccountingPersistenceConfiguration(PersistenceCouchDB.class);
			serviceEndpoint = persitenceConfiguration.getServiceEndpoint(
					AccountingPersistenceConfiguration.SERVICE_ENDPOINT_CATEGORY, 
					AccountingPersistenceConfiguration.SERVICE_ENDPOINT_NAME, 
					PersistenceCouchDB.class);
			SecurityTokenProvider.instance.set(TestUtility.TOKEN);
		}
		

		long startTime = Calendar.getInstance().getTimeInMillis();
		long endTime = startTime;
		while(endTime <=  (startTime + 10*1000)){ // 10 sec
			endTime = Calendar.getInstance().getTimeInMillis();
		}
		
		logger.debug("Going to check First Time");
		PersistenceBackend first = PersistenceBackendFactory.getPersistenceBackend(TestUtility.getScope());
		logger.debug("First {} : {}", PersistenceBackend.class.getSimpleName(), first);
		
		publishResource(serviceEndpoint);
		
		startTime = Calendar.getInstance().getTimeInMillis();
		endTime = startTime;
		while(endTime <=  (startTime + (PersistenceBackendFactory.FALLBACK_RETRY_TIME + 100))){
			endTime = Calendar.getInstance().getTimeInMillis();
		}

		logger.debug("Going to check Second Time");
		PersistenceBackend second = PersistenceBackendFactory.getPersistenceBackend(TestUtility.getScope());
		logger.debug("Second {} : {}", PersistenceBackend.class.getSimpleName(), second);
		
		Assert.assertNotEquals(first, second);
		
	}
	
	
	static final String FALLBACK_FILENAME = "fallback.log";

	//@Test
	public void testOld(){
		File newFile = new File(".", "_d4science.research-infrastructures.eu_gCubeApps_ScalableDataMining.fallback.log");
		
		String oldAccountingFileName = newFile.getName();
		int lastIndexOf_ = oldAccountingFileName.lastIndexOf("_");
		
		oldAccountingFileName = oldAccountingFileName.substring(lastIndexOf_+1);
		oldAccountingFileName = oldAccountingFileName.replace(
				FALLBACK_FILENAME, "accountingFallback.log");
		
		File oldAccountingFile = new File(newFile.getParentFile(), oldAccountingFileName);
		logger.debug("New Filename : {}\n Old Filename : {}", newFile, oldAccountingFile);
		
	}
}
*/