/**
 * 
 */
package org.gcube.accounting.persistence;

import java.io.StringWriter;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.Resources;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.documentstore.persistence.PersistenceBackend;
import org.gcube.documentstore.persistence.PersistenceBackendFactory;
import org.gcube.documentstore.persistence.PersistenceCouchBase;
import org.gcube.informationsystem.publisher.RegistryPublisher;
import org.gcube.informationsystem.publisher.RegistryPublisherFactory;
import org.gcube.testutility.ContextTest;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class PersistenceCouchBaseTest extends ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(PersistenceCouchBaseTest.class);
	
	public static final long timeout = 5000;
	public static final TimeUnit timeUnit = TimeUnit.MILLISECONDS;
	
	
	private static void publishResource(Resource resource) throws Exception {
		StringWriter stringWriter = new StringWriter();
		Resources.marshal(resource, stringWriter);
		
		RegistryPublisher registryPublisher = RegistryPublisherFactory.create();
		
	    try {
	    	logger.debug("Trying to publish to {}:\n{}",ContextTest.getCurrentContext(), stringWriter);
	    	registryPublisher.create(resource);
		} catch (Exception e) {
			logger.error("The resource was not published", e);
			throw e;
		}
	}
	
	private static void unPublishResource(Resource resource) throws Exception {
		//StringWriter stringWriter = new StringWriter();
		//Resources.marshal(resource, stringWriter);
		
		RegistryPublisher registryPublisher = RegistryPublisherFactory.create();
	    
		String id = resource.id();
	    logger.debug("Trying to remove {} with ID {} from {}", 
	    		resource.getClass().getSimpleName(), id, 
	    		ContextTest.getCurrentContext());
	    
		registryPublisher.remove(resource);
		
		logger.debug("{} with ID {} removed successfully", resource.getClass().getSimpleName(), id);
	}
	
	public void testScopeRecheck() throws Exception {
		ServiceEndpoint serviceEndpoint = null;
		try {
			AccountingPersistenceConfiguration persitenceConfiguration = new AccountingPersistenceConfiguration(PersistenceCouchBase.class);
			serviceEndpoint = persitenceConfiguration.getServiceEndpoint(
					AccountingPersistenceConfiguration.SERVICE_ENDPOINT_CATEGORY, 
					AccountingPersistenceConfiguration.SERVICE_ENDPOINT_NAME, 
					PersistenceCouchBase.class);
			unPublishResource(serviceEndpoint);
		}catch(IndexOutOfBoundsException e){
			String currentContext = ContextTest.getCurrentContext(); 
			ContextTest.setContextByName("/gcube/devsec");
			AccountingPersistenceConfiguration persitenceConfiguration = new AccountingPersistenceConfiguration(PersistenceCouchBase.class);
			serviceEndpoint = persitenceConfiguration.getServiceEndpoint(
					AccountingPersistenceConfiguration.SERVICE_ENDPOINT_CATEGORY, AccountingPersistenceConfiguration.SERVICE_ENDPOINT_NAME, 
					PersistenceCouchBase.class);
			ContextTest.setContextByName(currentContext);
		}
		
		long startTime = Calendar.getInstance().getTimeInMillis();
		long endTime = startTime;
		while(endTime <=  (startTime + 10*1000)){ // 10 sec
			endTime = Calendar.getInstance().getTimeInMillis();
		}
		
		logger.debug("Going to check First Time");
		PersistenceBackend first = PersistenceBackendFactory.getPersistenceBackend(ContextTest.getCurrentContext());
		logger.debug("First {} : {}", PersistenceBackend.class.getSimpleName(), first);
		
		publishResource(serviceEndpoint);
		
		startTime = Calendar.getInstance().getTimeInMillis();
		endTime = startTime;
		while(endTime <=  (startTime + (PersistenceBackendFactory.FALLBACK_RETRY_TIME + 100))){
			endTime = Calendar.getInstance().getTimeInMillis();
		}

		logger.debug("Going to check Second Time");
		PersistenceBackend second = PersistenceBackendFactory.getPersistenceBackend(ContextTest.getCurrentContext());
		logger.debug("Second {} : {}", PersistenceBackend.class.getSimpleName(), second);
		
		Assert.assertNotEquals(first, second);
		
	}
	
}
