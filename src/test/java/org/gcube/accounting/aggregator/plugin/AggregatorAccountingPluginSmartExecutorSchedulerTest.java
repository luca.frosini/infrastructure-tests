/**
 * 
 */
package org.gcube.accounting.aggregator.plugin;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.gcube.accounting.aggregator.aggregation.AggregationType;
import org.gcube.accounting.aggregator.plugin.AccountingAggregatorPlugin.ElaborationType;
import org.gcube.accounting.aggregator.utility.Utility;
import org.gcube.accounting.datamodel.usagerecords.ServiceUsageRecord;
import org.gcube.accounting.datamodel.usagerecords.StorageUsageRecord;
import org.gcube.testutility.ContextTest;
import org.gcube.vremanagement.executor.api.types.LaunchParameter;
import org.gcube.vremanagement.executor.api.types.Scheduling;
import org.gcube.vremanagement.executor.client.SmartExecutorClient;
import org.gcube.vremanagement.executor.client.SmartExecutorClientFactory;
import org.gcube.vremanagement.executor.json.SEMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AggregatorAccountingPluginSmartExecutorSchedulerTest extends ContextTest {

	private static Logger logger = LoggerFactory.getLogger(AggregatorAccountingPluginSmartExecutorSchedulerTest.class);

	private SmartExecutorClient smartExecutorClient;

	@Before
	public void before() throws Exception {
		setContext("/gcube");
		smartExecutorClient = SmartExecutorClientFactory.create(AccountingAggregatorPluginDeclaration.NAME);
		Assert.assertNotNull(smartExecutorClient);

	}
	
	private void launch(Scheduling scheduling, Map<String, Object> inputs) throws Exception {

		LaunchParameter launchParameter = new LaunchParameter(AccountingAggregatorPluginDeclaration.NAME, inputs);
		launchParameter.setScheduling(scheduling);

		try {
			String uuidString = smartExecutorClient.launch(SEMapper.marshal(launchParameter));
			logger.debug("Launched with UUID : {}", uuidString);
		} catch (Exception e) {
			logger.error("Error while launching {}", e);
			throw e;
		}
	}
	
	private Map<String, Object> getAggregateInputs() throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();

		inputs.put(AccountingAggregatorPlugin.AGGREGATION_TYPE_INPUT_PARAMETER, AggregationType.MONTHLY.name());
		
		inputs.put(AccountingAggregatorPlugin.ELABORATION_TYPE_INPUT_PARAMETER, ElaborationType.AGGREGATE.name());
		
		
		inputs.put(AccountingAggregatorPlugin.PERSIST_START_TIME_INPUT_PARAMETER, Utility.getPersistTimeParameter(3, 0));
		inputs.put(AccountingAggregatorPlugin.PERSIST_END_TIME_INPUT_PARAMETER, Utility.getPersistTimeParameter(19, 00));
		
		
		inputs.put(AccountingAggregatorPlugin.RECORD_TYPE_INPUT_PARAMETER, StorageUsageRecord.class.newInstance().getRecordType());
		
		Calendar aggregationStartCalendar = Utility.getAggregationStartCalendar(2016, Calendar.FEBRUARY, 1);
		String aggregationStartDate = AccountingAggregatorPlugin.AGGREGATION_START_DATE_DATE_FORMAT.format(aggregationStartCalendar.getTime());
		logger.trace("{} : {}", AccountingAggregatorPlugin.AGGREGATION_START_DATE_INPUT_PARAMETER, aggregationStartDate);
		inputs.put(AccountingAggregatorPlugin.AGGREGATION_START_DATE_INPUT_PARAMETER, aggregationStartDate);
		
		inputs.put(AccountingAggregatorPlugin.RESTART_FROM_LAST_AGGREGATION_DATE_INPUT_PARAMETER, true);

		return inputs;
	}

	@Test
	public void aggregate() throws Exception {
		// Every 5 minutes
		CronExpression cronExpression = new CronExpression("0 0/5 * 1/1 * ? *");
		Scheduling scheduling = new Scheduling(cronExpression, true);
		scheduling.setGlobal(false);

		Map<String, Object> inputs = getAggregateInputs();
		
		launch(null, inputs);

	}
	

	
	private Map<String, Object> getMonthlyAggregateInputs() throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		inputs.put(AccountingAggregatorPlugin.AGGREGATION_TYPE_INPUT_PARAMETER, AggregationType.MONTHLY.name());

		inputs.put(AccountingAggregatorPlugin.ELABORATION_TYPE_INPUT_PARAMETER, ElaborationType.AGGREGATE.name());

		inputs.put(AccountingAggregatorPlugin.PERSIST_START_TIME_INPUT_PARAMETER, Utility.getPersistTimeParameter(3, 0));
		inputs.put(AccountingAggregatorPlugin.PERSIST_END_TIME_INPUT_PARAMETER, Utility.getPersistTimeParameter(19, 00));

		inputs.put(AccountingAggregatorPlugin.RECORD_TYPE_INPUT_PARAMETER,
				ServiceUsageRecord.class.newInstance().getRecordType());

		Calendar aggregationStartCalendar = Utility.getAggregationStartCalendar(2017, Calendar.SEPTEMBER, 1);
		String aggregationStartDate = AccountingAggregatorPlugin.AGGREGATION_START_DATE_DATE_FORMAT.format(aggregationStartCalendar.getTime());
		logger.trace("{} : {}", AccountingAggregatorPlugin.AGGREGATION_START_DATE_INPUT_PARAMETER, aggregationStartDate);
		inputs.put(AccountingAggregatorPlugin.AGGREGATION_START_DATE_INPUT_PARAMETER, aggregationStartDate);
		/*
		Calendar aggregationEndCalendar = Utility.getAggregationStartCalendar(2017, Calendar.JUNE, 30);
		String aggregationEndDate = AccountingAggregatorPlugin.AGGREGATION_START_DATE_DATE_FORMAT.format(aggregationEndCalendar.getTime());
		logger.trace("{} : {}", AccountingAggregatorPlugin.AGGREGATION_START_DATE_INPUT_PARAMETER, aggregationEndDate);
		inputs.put(AccountingAggregatorPlugin.AGGREGATION_END_DATE_INPUT_PARAMETER, aggregationEndDate);
		*/
		
		inputs.put(AccountingAggregatorPlugin.RESTART_FROM_LAST_AGGREGATION_DATE_INPUT_PARAMETER, true);

		return inputs;
	}

	// @Test
	public void aggregateMonthly() throws Exception {
		CronExpression cronExpression = new CronExpression("0 0 10 1/1 * ? *");
		Scheduling scheduling = new Scheduling(cronExpression, true);
		scheduling.setGlobal(false);
		
		ContextTest.setContextByName("/gcube");
		Map<String, Object> inputs = getMonthlyAggregateInputs();
		launch(scheduling, inputs);
	}
	
	
	/* ----------------------------------------------------------------------------------- */
	
	
	private Map<String, Object> getRecoveryInputs() throws Exception {
		Map<String, Object> inputs = new HashMap<String, Object>();
		inputs.put(AccountingAggregatorPlugin.ELABORATION_TYPE_INPUT_PARAMETER, ElaborationType.RECOVERY.name());

		inputs.put(AccountingAggregatorPlugin.PERSIST_START_TIME_INPUT_PARAMETER, Utility.getPersistTimeParameter(3, 0));
		inputs.put(AccountingAggregatorPlugin.PERSIST_END_TIME_INPUT_PARAMETER, Utility.getPersistTimeParameter(19, 0));

		
		Calendar aggregationStartCalendar = Utility.getAggregationStartCalendar(2017, Calendar.AUGUST, 1);
		String aggregationStartDate = AccountingAggregatorPlugin.AGGREGATION_START_DATE_DATE_FORMAT.format(aggregationStartCalendar.getTime());
		logger.trace("{} : {}", AccountingAggregatorPlugin.AGGREGATION_START_DATE_INPUT_PARAMETER, aggregationStartDate);
		inputs.put(AccountingAggregatorPlugin.AGGREGATION_START_DATE_INPUT_PARAMETER, aggregationStartDate);
		
		/*
		Calendar aggregationEndCalendar = Utility.getAggregationStartCalendar(2017, Calendar.JUNE, 22);
		String aggregationEndDate = AccountingAggregatorPlugin.AGGREGATION_START_DATE_DATE_FORMAT.format(aggregationEndCalendar.getTime());
		logger.trace("{} : {}", AccountingAggregatorPlugin.AGGREGATION_START_DATE_INPUT_PARAMETER, aggregationEndDate);
		inputs.put(AccountingAggregatorPlugin.AGGREGATION_END_DATE_INPUT_PARAMETER, aggregationEndDate);
		*/
		
		return inputs;
	}

	// @Test
	public void recovery() throws Exception {
		// Every Day at 8:00
		CronExpression cronExpression = new CronExpression("0 0/5 3-19 1/1 * ? *");
		Scheduling scheduling = new Scheduling(cronExpression, true);
		scheduling.setGlobal(false);

		Map<String, Object> inputs = getRecoveryInputs();

		launch(scheduling, inputs);

	}

	@Test
	public void unSchedule() throws Exception {
		smartExecutorClient.delete("d06fc539-af83-4dae-a274-c721b4710fcd", true);
	}

	@Test
	public void stop() throws Exception {
		smartExecutorClient.delete("91178b6e-811f-410d-9cff-ef7b4a7a2475", true);
	}

}
